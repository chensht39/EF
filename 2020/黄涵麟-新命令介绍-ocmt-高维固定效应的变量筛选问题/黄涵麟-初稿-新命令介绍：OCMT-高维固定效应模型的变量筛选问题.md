&#x1F449;  点击右上方的【编辑】按钮，可以查看 Markdown 原始文档。

&emsp;

> **作者：** 黄涵麟(中山大学)        
> **E-mail:**  <huanghlin5@mail2.sysu.edu.cn>   

&emsp;

---

**目录**
[[TOC]]

---

&emsp;

本篇推文介绍 Stata 中用于多维环境下的模型选择的命令， ``` ocmt ```。该命令的主要目的是替代惩罚回归，在高维回归的环境下进行模型选择，并且不包含任何噪声变量。该变量筛选工具的计算速度快，在消除噪声变量和拾取信号方面相当强大。```ocmt```在小样本时表现良好，在一些实验当中其性能优于 Lasso 和 Adaptive Lasso 方法。



## 1. 什么是 OCMT ？

OCMT（A One Covariate at A Time, Multiple Testing Approach to Variable Selection in High-Dimensional Linear Regression Models）指的是高维线性回归模型中变量选择的一次一个协变量、多重检验方法，是一种模型选择装置。

OCMT 一次一个地测试$n$个可用潜在协变量在单独解释$y_t$时的净贡献的统计显著性，同时充分考虑问题的多重测试性质。具有统计显著净贡献的所有协变量将被联合选择，以形成$y_t$的初始模型。

OCMT 在一个步骤当中选择所有$t$值超过临界值的协变量，只有当存在隐藏信号（Hidden-Signal）时才需要第二阶段。

&emsp;

## 2. 理论基础

### 2.1 变量的筛选

考虑模型
$$
y_t=a'z_t+\sum_{t=1}^k\beta_ix_{it}+u_t
$$

>  $z_t$ 为预选变量向量，$x_{it}$ 为 $k$ 个未知的真实变量或信号变量，$u_t$ 为误差项

> $z_t$、$x_{it}$ 与 $u_t$ 不相关

> $z_t$ 可以包括确定项 ( 如常数、虚拟变量和线性趋势 ) 和/或随机变量 ( 包括 $y_t$ 的公共因子和滞后值)  。

我们将$n$个可用潜在协变量称为一个**活动集**，其中包含了**真实模型的$k$个信号变量**，还有噪声变量和剩余变量。
$$
S_n=\{x_{it}，i=1,2\cdots,n\}
$$

> 噪声变量：与信号的相关性为0；

> 剩余变量：除去 $z_t$ 后与信号相关，称之为**伪信号**或**代理变量**，他们可能被错误地视为信号变量。

OCMT 将用 $y_t$ 对 $z_t$ 和**活动集**中的 $x_{it}$ **一次一个**地进行回归，并且分别求出 $x_{it}$ 的 $t$ 值 **$t_i$**。
$$
\begin{aligned}
t_i &=\frac{T^{-1/2}x'_iM_zy}{\hat\sigma_i\sqrt{T^{-1}x'_iM_zx_i}} =\frac{T^{-1/2}x'_iM_z\mu}{\hat\sigma_i\sqrt{T^{-1}x'_iM_zx_i}}+\frac{T^{-1/2}x'_iM_zu}{\hat\sigma_i\sqrt{T^{-1}x'_iM_zx_i}} \\
&=t_{i,\mu}+t_{i,u}
\end{aligned}
$$

> $x_i=(x_{i1},x_{i2},\cdots,x_{iT})'$，$y=(y_{1},y_{2},\cdots,y_{T})'$

> $\mu=(\mu_{1},\mu_{2},\cdots,\mu_{T})'$，$\mu_t=\sum_{i=1}^k\beta_ix_{it}$，$u=(u_{1},u_{2},\cdots,u_{T})'$ 

> $M_z=I_t-Z(Z'Z)^{-1}Z'$ ，$Z=（z_1,z_2,\cdots,z_T）'$

> $\hat\sigma_i$是该回归的标准误

当 $n,T\rightarrow\infin$ 时，我们依赖 $t_{i,u}$ 保持足够的概率有界来使我们能够在 $n$ 很大时进行多次测试。我们依靠判断当 $n,T\rightarrow\infin$ 时， $t_{i,\mu}$ 在概率上是否保持足够的概率有界来区分信号变量。

+ **当 $t_{i,\mu}$ 概率有界时，如果 $x_{it}$ 不包含在 $\mu_t$ 中，则其为噪声变量；若包含在 $\mu_t$ 中，则为隐藏变量**
+ **当 $t_{i,\mu}$ 概率无界时，如果 $x_{it}$ 不包含在 $\mu_t$ 中，则其为伪信号变量；若包含在 $\mu_t$ 中，则为信号变量**

&emsp;

OCMT的变量选择方法侧重于以预选变量 $z_t$ 为条件的 $x_{it}$ 对 $y_t$ 的净影响，而不是由 $\beta_i$ 定义的边际效应。因此，考虑 [Pesaran和Smith ( 2014 ) ](http://www.e-publications.org/srv/ecta/linkserver/openurl?rft_dat=bib:32/pes2014&rfe_id=urn:sici%2F0012-9682%28201807%2986%3A4%3C1479%3AAOCAAT%3E2.0.CO%3B2-V) 提出的平均净影响系数 ( Mean Net Impact Coefficient ) ，则 $x_{it}$ 对 $y_t$ 的净影响可以表示为：
$$
\theta_{i,T}(z)=\sum_{j=1}^k\beta_j\sigma_{ij,T}(z)
$$

> $\sigma_{it,T}=E(T^{-1}x'_iM_zx_j)$

为了简化表达，我们去掉下标 $T$ 并用 $\theta_i(z)$ 和 $\sigma_{ij}(z)$ 来表示。

$\theta_i(z)$ 决定了 $t_{i,\mu}$ 在 $n,T\rightarrow\infin$ 时是否足够概率有界，我们依靠 $\theta_i(z)$ 来选择变量，但 $\theta_i(z)\ne0$ 不一定意味着 $\beta\ne0$ 。同样，我们拥有以下四种可能的情况：

|               |       $\theta_i(z)\ne0$        | $\theta_i(z)=0$ |
| :-----------: | :----------------------------: | :-------------: |
| $\beta_i\ne0$ | (I) 信号变量，拥有非零的净影响 |  (II) 隐藏变量  |
|  $\beta_i=0$  |        (III) 伪信号变量        |  (IV) 噪声变量  |

情况 (II) 的出现比较少见，但是伪信号情况 (III) 的出现是很有可能的，伪信号是模型选择策略中一个重要的考虑因素。

我们将只包含信号的模型叫做**真实模型**，把包含了伪信号变量，但不包含噪声变量的模型称为**近似模型**。

&emsp;

### 2.2 OCMT 的过程

#### 2.2.1 模型设定

考虑 $n$ 个二元模型 ( 这里我们使得预选变量 $z_t=1$ 以简化模型) ：
$$
y_t=c_i+\phi_ix_{it}+u_{it}
$$

> 其中 $\phi_i=\theta_i/\sigma{ii}$ ，$\theta_i$ 是条件净影响系数 $\theta_i=\sum_{j=1}^k\beta_j\sigma_{ij}$，$\sigma_{ij}=E(T^{-1}x'_iM_\tau)x_j$

可以用矩阵表示为：
$$
y=a\tau_T+X_k\beta_k + u
$$

>  $\tau_T$ 是1的 $T×1$ 向量

>  $X_k=(x_1,x_2,\cdots,x_k)$ 是信号变量的 $T×k$ 观测矩阵

> $\beta_k=(\beta_1,\beta_2,\cdots,\beta_k)'$ 是 $k×1$ 相关斜率系数向量

得到最小二乘回归估计量 $\hat\phi_i$ 的 $t$ 值为：
$$
t_{\hat\phi_{i,(1)}}=\frac{\hat\phi_i}{s.e.(\hat\phi_i)}=\frac{x'_iM_\tau y }{\hat\sigma_i \sqrt{x'_iM_\tau x_i}}
$$

> $\hat\phi_i=(x'_iM_\tau x_i)_{-1}x'_iM_\tau y$



#### 2.2.2 关键值函数设定

设置关键值函数为：
$$
c_p(n,\delta)=\phi^{-1}(1-\frac{p}{2f(n,\delta)})
$$

> 其中 $\phi^{-1}(·)$ 是标准正态分布的反函数； $f(n,\delta)=cn^\delta$ ，$n$ 为活动集中的变量个数，Stata 程序中$c=1$

研究者需要根据情况设置**关键值指数** $\delta$ 和 $p-value$ ，其中 $\delta$  需要设置2个 ( $\delta$ 和 $\delta^*$ ，且 $\delta_2$ 必须大于 $\delta_1$ ) ，$\delta$ 和 $p$ 值的设定对于 OCMT 过程十分重要，因为它们用于设定第一阶段 ( $\delta$ ) 和后续阶段 ( $\delta^*$ ) 中一次一个地对活动集中变量进行最小二乘回归中 $t$ 值的阈值。

在第一阶段中设置 $\delta$ 的值为1相当于对多重测试问题使用 Bonferroni 校正。



#### 2.2.3 第一阶段

**第一阶段 ( $j=1$ ) **，依靠选择指标：
$$
\hat\Gamma_{i,(1)}=I[|t_{\hat\phi_{i,(1)}}|>c_p(n,\delta)]\quad i=1,2,\cdots,n
$$
若 $\hat\Gamma_{1,(1)}=1$ ，则在第一阶段将 $x_i$ 选择为信号变量和伪信号变量，从**活动集**中取出，放入**索引集**。



#### 2.2.4 后续阶段

**后续阶段 ( $j=2,3,\cdots$ ) **，我们使用 $y_t$ 对**上一阶段筛选后活动集中剩余的变量**再次一次一个地进行最小二乘回归，同样地计算出 $t$ 值：
$$
t_{\hat\phi_{i,(j)}}=\frac{\hat\phi_{i,(j)}}{s.e.（\hat\phi_{i,(j)}）}=\frac{x'_iM_{(j-1)}y}{\hat\sigma_{i,(j)}\sqrt{x'_iM_{(j-1)} x_i}}
$$

> 其中 $i$ 为**上一阶段筛选后活动集中剩余的变量的下标**， $j=2,3,\cdots,$

后续阶段的选择指标为 ( 关键值函数采用 $\delta^*$ ) ：
$$
\hat\Gamma_{i,(j)}=I[|t_{\hat\phi_{i,(j)}}|>c_p(n,\delta^*)]
$$
**当在给定阶段没有选出信号变量和伪信号变量时，OCMT 过程终止。**否则， $j=j+1$ ，程序继续。

&emsp;

## 3. Stata 实操

### 3.1 命令安装

```stata
findit ocmt   ///查找ocmt命令
```

选择命令的链接：

![image-20201213181834413](C:\Users\dell\AppData\Roaming\Typora\typora-user-images\image-20201213181834413.png)

点击 **click here to install**安装命令即可。

![image-20201213182006591](C:\Users\dell\AppData\Roaming\Typora\typora-user-images\image-20201213182006591.png)

&emsp;

### 3.2 命令语法

```stata
ocmt depar [indepvars] [if] [in] [, options]
```

```depvar```：因变量

```indepvars```：解释变量

**[,options]** ：可以选择预选变量 ( $z_t$ ) 、设定关键值函数的 $p$ 值、 $\delta$ 和 $\delta^*$ 。

> 在 Stata 的程序中， $\delta$ 的默认值为1， $\delta^*$ 的默认值为2 ( Chudik et al. ( 2018 ) 选择 $\delta^*$=1.5 ) ， $p$ 的默认值为5%。



### 3.3 命令操作

+ 数据生成

  ```stata
  set obs 50
  set seed 123
  gen t = _n
  gen x1 = rnormal()
  gen x2 = rnormal()
  gen x3 = rnormal()
  gen x4 = rnormal()
  gen x5 = rnormal()
  gen x6 = rnormal()
  gen x7 = rnormal()
  gen x8 = rnormal()
  gen x9 = rnormal()
  gen x10 = rnormal()
  gen z1 = rnormal()
  gen w1 = rnormal()
  gen y = 2 + 0.8*x1 + x2 - 3*x5 + x8 - 2*z1 + rnormal() 
  ```

  

+ 设定时序

  ```stata
  tsset t
  ```

  **Note : 使用 ```ocmt``` 命令之前必须对数据先使用 ```tsset``` 命令！**

  

+ 使用 ```ocmt``` 筛选变量

  ```stata
  ocmt y x* w1 z1, sig(10) delta1(1) delta2(2)
  ```

  > 注意：置信水平用整数表示，若想设置 $p$ 值为 5%，则输入 ```sig(5)``` ，而非 ```sig(5%)``` 。

  由于 $\delta$ 默认值为1，$\delta^*$ 默认值为2，所以上面的命令也等效于：

  ```stata
  ocmt y x* w1 z1, sig(10)
  ```

  得到输出结果如下：

  ```stata
  Dependent variable: y
  Active set: x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 w1 z1
  Number of variables in active set = 12
  Pvalue =         .1
  delta_1 =       1
  delta_2 =       2
  Pvalue_1 =       .1
  Pvalue_2 =       .00833333
  t_threshold_1 = 2.6382573
  t_threshold_2 = 3.3917631
  Preselected variables: Constant
  Variables chosen in stage 1
  x5 z1
  Variables chosen in stage 2
  x5 z1 x1 x2 x8
  Variables chosen in stage 3
  x5 z1 x1 x2 x8
  
  ------------------------------------------------------------------------------
  ------------------------------------------------------------------------------
  One Covariate at a Time Multiple Testing (OCMT)
  Chosen model after 2 stages
  ------------------------------------------------------------------------------
  
        Source |       SS           df       MS      Number of obs   =        50
  -------------+----------------------------------   F(5, 44)        =    260.09
         Model |  723.688406         5  144.737681   Prob > F        =    0.0000
      Residual |  24.4858279        44  .556496088   R-squared       =    0.9673
  -------------+----------------------------------   Adj R-squared   =    0.9636
         Total |  748.174234        49  15.2688619   Root MSE        =    .74599
  
  ------------------------------------------------------------------------------
             y |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
  -------------+----------------------------------------------------------------
            x5 |  -2.821404   .1210628   -23.31   0.000     -3.06539   -2.577418
            z1 |  -2.100404   .0992257   -21.17   0.000     -2.30038   -1.900427
            x1 |    .866931    .116969     7.41   0.000     .6311954    1.102667
            x2 |   .8881723   .0994029     8.94   0.000      .687839    1.088506
            x8 |   .8268681   .0986016     8.39   0.000     .6281497    1.025587
         _cons |   2.308552   .1077874    21.42   0.000     2.091321    2.525783
  ------------------------------------------------------------------------------
  ```

  > Pvalue 为选项中设定的 $p$ 值，delta_1 和 delta_2 是第一阶段和后续阶段的关键值指数 $\delta$ 、$\delta^*$。

  > Pvalue_1 和 Pvalue_2 的计算公式为：$p_1=p/n^{\delta-1}$ 、$p_1=p/n^{\delta^*-1}$。

  > t_threshold_1 和 t_threshold_2 分别是第一阶段和后续阶段的关键值函数的值。

  > 预选变量将会出现在 Preselected Variables 后，此次没有选择预选变量，固显示为 Constant 。

  在第一阶段的筛选中选出了变量 ```x5``` 和变量 ```z1``` ，第二阶段又将 ```x1```、 ```x2``` 和 ```x8``` 放入了索引集。第三阶段索引集中不再有新增的信号变量和伪信号变量，因此程序在第三阶段终止。我们经过两个阶段筛选出了索引集中的变量，```ocmt``` 会将 ```y``` 对索引集中的所有变量进行回归并将结果附在最后。

  

+ 加入预选变量 $z_1$

  如果我们加入预选变量 $z_1$，则在选项中输入 ```z(z1)``` 。

  ```stata
  ocmt y x* w1, z(z1) sig(5) delta1(1) delta2(2)
  ```

  由于我们选择的显著水平和关键值指数均为默认值 ，因此可以省略。上述命令等价于：

  ```stata
  ocmt y x* w1, z(z1)
  ```

  输出结果如下：

  ```stata
  Dependent variable: y
  Active set: x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 w1
  Number of variables in active set = 11
  Pvalue =         .05
  delta_1 =       1
  delta_2 =       2
  Pvalue_1 =       .05
  Pvalue_2 =       .00454545
  t_threshold_1 = 2.8375969
  t_threshold_2 = 3.5314915
  Preselected variables (apart from constant): z1
  Variables chosen in stage 1
  x5
  Variables chosen in stage 2
  x5 x1 x2 x8
  Variables chosen in stage 3
  x5 x1 x2 x8
  
  ------------------------------------------------------------------------------
  ------------------------------------------------------------------------------
  One Covariate at a Time Multiple Testing (OCMT)
  Chosen model after 2 stages
  ------------------------------------------------------------------------------
  
        Source |       SS           df       MS      Number of obs   =        50
  -------------+----------------------------------   F(5, 44)        =    260.09
         Model |  723.688406         5  144.737681   Prob > F        =    0.0000
      Residual |  24.4858279        44  .556496088   R-squared       =    0.9673
  -------------+----------------------------------   Adj R-squared   =    0.9636
         Total |  748.174234        49  15.2688619   Root MSE        =    .74599
  
  ------------------------------------------------------------------------------
             y |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
  -------------+----------------------------------------------------------------
            x5 |  -2.821404   .1210628   -23.31   0.000     -3.06539   -2.577418
            x1 |    .866931    .116969     7.41   0.000     .6311954    1.102667
            x2 |   .8881723   .0994029     8.94   0.000      .687839    1.088506
            x8 |   .8268681   .0986016     8.39   0.000     .6281497    1.025587
            z1 |  -2.100404   .0992257   -21.17   0.000     -2.30038   -1.900427
         _cons |   2.308552   .1077874    21.42   0.000     2.091321    2.525783
  ------------------------------------------------------------------------------
  ```



+ 加入滞后项

  我们可以在命令中加入 ```x``` 的滞后项。

  ```stata
  ocmt y x* l.x* w1, z(z1) delta1(1) delta2(2)
  ```

  结果输出如下：

  ```stata
  Significance level not specified. Using default value
  Dependent variable: y
  Active set: x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 L.x1 L.x2 L.x3 L.x4 L.x5 L.x6 L.x7 L.x8 L.x9 L.x10 w1
  Number of variables in active set = 21
  Pvalue =         .05
  delta_1 =       1
  delta_2 =       2
  Pvalue_1 =       .05
  Pvalue_2 =       .00238095
  t_threshold_1 = 3.0380743
  t_threshold_2 = 3.860018
  Preselected variables (apart from constant): z1
  Variables chosen in stage 1
  x5
  Variables chosen in stage 2
  x5 x1 x2 x8
  Variables chosen in stage 3
  x5 x1 x2 x8
  
  ------------------------------------------------------------------------------
  ------------------------------------------------------------------------------
  One Covariate at a Time Multiple Testing (OCMT)
  Chosen model after 2 stages
  ------------------------------------------------------------------------------
  
        Source |       SS           df       MS      Number of obs   =        49
  -------------+----------------------------------   F(5, 43)        =    254.83
         Model |  712.075427         5  142.415085   Prob > F        =    0.0000
      Residual |  24.0308011        43  .558855839   R-squared       =    0.9674
  -------------+----------------------------------   Adj R-squared   =    0.9636
         Total |  736.106228        48  15.3355464   Root MSE        =    .74757
  
  ------------------------------------------------------------------------------
             y |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
  -------------+----------------------------------------------------------------
            x5 |  -2.812163   .1217507   -23.10   0.000    -3.057697    -2.56663
            x1 |   .8658339   .1172231     7.39   0.000     .6294311    1.102237
            x2 |   .8852265   .0996669     8.88   0.000      .684229    1.086224
            x8 |   .8345154   .0991732     8.41   0.000     .6345135    1.034517
            z1 |  -2.129791   .1046334   -20.35   0.000    -2.340804   -1.918777
         _cons |   2.297156   .1087515    21.12   0.000     2.077838    2.516474
  ------------------------------------------------------------------------------
  ```



+ 存储的结果

  除了在最后的回归中命令 ```regress``` 储存的结果之外， ```ocmt``` 还额外储存了**过程的阶段数**、**关键值函数值**和**选出的回归元** ( 包含预选变量 ) 。

  我们可以使用 ```display``` 命令将 ```ocmt``` 命令过程当中储存的结果列出，例如：

  ```stata
  display r(regressors)
  display r(stages)
  display r(threshold1)
  display r(threshold2)
  ```

&emsp;

## 4.结语

本篇推文主要从理论基础和 Stata 的实际操作两个方面详细介绍了 Stata 命令 ```ocmt``` ，它为我们提供了一种基于多重测试的计算简单、快速、有效的稀疏的回归函数变量的替代方法。```ocmt``` 的程序较为简单，理解起来也较为容易，但是其能力却十分强大，可谓短小精悍。

&emsp;

## 参考文献

+ Chudik A., G . Kapetanios and M. H. Pesaran. 2018. A One Covariate at a Time, Multiple Testing Approach to Variale Selection in High-Dimensional Linear Regression Models. Econometrica 86: 1479-2512. [-PDF-](https://onlinelibrary.wiley.com/doi/pdf/10.3982/ECTA14176)
+ Pesaran M H , Smith R P . Signs of impact effects in time series regression models[J]. Economics Letters, 2014, 122(2):150-153.[-PDF-](https://www.baidu.com/link?url=Mv3-YT8jNLcSW78yJJFXvkbPWhE90oz5FOgi-Vjz0eKYA26SDXyVd2E6pEemEGEX4nalcDJAsdlpcHoZ-H6bwzUYi0NeaiH1dvsOFrIyOQK&wd=&eqid=9b997ffc00071172000000065fd5e986)



## 5. 前期相关推文

> Note：如下内容可以使用 `lianxh 论文, m` 命令自动生成。执行 `ssc install lianxh` 可以下载 `lianxh` 命令。 

- 专题：[论文写作](https://www.lianxh.cn/blogs/31.html)
  - [金融领域引用率最高的50篇论文](https://www.lianxh.cn/news/1b0b0a25b5133.html)
  - [论文写作：Word技巧和快捷键](https://www.lianxh.cn/news/ef46ef36d70f2.html)
  - [连享会：论文重现网站大全](https://www.lianxh.cn/news/e87e5976686d5.html)
  - [毕业季：不要提交一篇相貌丑陋的论文](https://www.lianxh.cn/news/a5445d129b272.html)
- 专题：[Stata资源](https://www.lianxh.cn/blogs/35.html)
  - [Stata Journal：2020年第3期论文(SJ 20-3)](https://www.lianxh.cn/news/54ca4828ff912.html)
  - [Stata Journal 2020年第2期论文](https://www.lianxh.cn/news/28e9bee202473.html)
  - [会计期刊论文的结果可重现吗？](https://www.lianxh.cn/news/ef7c48dbc31da.html)
- 专题：[结果输出](https://www.lianxh.cn/blogs/22.html)
  - [baselinetable命令：论文基本统计量表格输出到Excel和Word](https://www.lianxh.cn/news/8635f14e51018.html)
  - [Stata：毕业论文大礼包 A——实证结果输出命令大比拼](https://www.lianxh.cn/news/e4bd07ff2d243.html)
  - [Stata：毕业论文大礼包 C——新版 esttab](https://www.lianxh.cn/news/5e8d35cd460d7.html)
  - [君生我未生！Stata---论文四表一键出](https://www.lianxh.cn/news/0764bf4c5f32b.html)
  - [Stata：毕业论文大礼包 B——神速实证结果输出之搜狗短语](https://www.lianxh.cn/news/355cf4698c5bd.html)
  - [Stata：一文搞定论文表1——基本统计量列表](https://www.lianxh.cn/news/7ca2b65c68835.html)
- 专题：[IV-GMM](https://www.lianxh.cn/blogs/38.html)
  - [IV：可以用内生变量的滞后项做工具变量吗？](https://www.lianxh.cn/news/c45f6113e2c2a.html)
  - [IV专题: 内生性检验与过度识别检验](https://www.lianxh.cn/news/cbb06931b699e.html)
- 专题：[倍分法DID](https://www.lianxh.cn/blogs/39.html)
  - [长差分：Long Difference及Acemoglu AER论文推介](https://www.lianxh.cn/news/35f95001f7da8.html)
  - [Big Bad Banks：多期 DID 经典论文介绍](https://www.lianxh.cn/news/42611191cca93.html)
- 专题：[内生性-因果推断](https://www.lianxh.cn/blogs/19.html)
  - [Stata：内生变量的交乘项如何处理？](https://www.lianxh.cn/news/603e0b458bc07.html)
  - [第三种内生性：衡量偏误(测量误差)如何检验-dgmtest？](https://www.lianxh.cn/news/44fa0eb361042.html)
  - [Stata新命令：konfound - 因果推断的稳健性检验](https://www.lianxh.cn/news/4832e3735dc81.html)
- 专题：[交乘项-调节](https://www.lianxh.cn/blogs/21.html)
  - [Stata：内生变量和它的交乘项](https://www.lianxh.cn/news/9056df610bb6c.html)


