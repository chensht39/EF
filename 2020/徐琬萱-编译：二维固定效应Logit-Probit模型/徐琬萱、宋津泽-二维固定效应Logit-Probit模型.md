&emsp;

\> **作者**：徐琬萱 宋津泽(中山大学)

\> **E-Mail:** <sodawine7@163.com>

&emsp;

本文编译自：

「 \*\*Source [ Mario Cruz-Gonzalez, Iván Fernández-Val, Martin Weidner, 2017, Bias Corrections for Probit and Logit Models with Two-way Fixed Effects, Stata Journal, 17(3): 517–545. ]」

编者按：在本文中，作者介绍了用户编写的命令`probitfe`和 `logitfe`，它们适用于具有个体和时间不可观测效应的 probit 和 logit 面板数据模型。由于附带的参数问题，估计未观测到的效应的固定效应面板数据方法可能存在严重偏差 ( Neyman and Scott, 1948, _Econometrica_ 16: 1–32 ) 。所以，他们使用 Fernández-Val 和 Weidner 对二维（_N_ 和 _T_）适度大的面板推导的分析和刀切法偏差校正 ( 2016, _Journal of Econometrics_ 192: 291–312 ) 来解决这个问题。同时，他们在本文中利用国际贸易的经验应用和蒙特卡洛模拟对命令进行说明。



**目录**

[toc]

## 1. 背景介绍

### 1.1 什么是固定效应模型及其不足

面板数据由一组个体在一段时间内的多个观察值组成，通常用于经验分析，以控制未观察到的个体和时间异质性。研究人员通常通过将个体和时间效应添加到模型中，并将这些未观察到的效应作为参数，在所谓的固定效应(FEs)方法中进行估计。

然而，非线性模型的固定效应估计存在附带参数问题 (Neyman and Scott 1948). 一个特殊的例子是具有个体效应的 logit 模型——其中可以使用条件似然方法 (Rasch 1960; Andersen, 1973; Chamberlain, 1984) 。这种方法提供了模型系数的估计，但不适用于概率单位模型，也不能产生平均部分效应(APE)或边际效应的估计，后者通常是二元响应模型中感兴趣的量。

此外，当面板很长并且模型也包含时间效应时，`clogit`和 `xtlogit`不能很好地工作，估计时间效应会引入额外的附带参数偏差。在实证分析中，时间效应通常被用来控制总体的共同冲击，并谨慎地考虑横截面相关性。

### 1.2 解决方法

在文章中，作者使用 Fernández-Val and Weidner (2016) 为双向固定效应非线性面板模型开发的偏差校正来处理附带参数问题。这些校正适用于二维(_N_ 和 _T_)适度较大的面板数据集或其他伪面板数据结构；比如 Arellano and Hahn (2007) 和 Fernández-Val and Weidner (2017) 关于处理附带参数问题的偏差校正方法的调查。

中等长度面板数据集的例子包括传统的微观经济面板调查，其数据历史悠久，如收入动态面板研究和全国青年纵向调查，国际跨国面板，如 PennWorld 表，美国州级面板，如当前人口调查，以及跨国家贸易流量的方形伪面板，如芬斯特拉的世界贸易流量和 CEPII，这些指数对应于与进出口商索引相同的国家。

`probitfe`和 `logitfe`命令可以对具有个体和时间效应的 logit 和 probit 模型的固定效应模型估计量进行分析和折刀校正。它们产生模型系数和 APE 的修正估计。



## 2.二维固定效应 Logit-Probit 模型

### 2.1 模型与估计量

考虑 Logit-Probit 模型,

$$
\operatorname{Pr}\left(Y_{i t}=1 \mid \mathbf{X}_{i}^{t}, \boldsymbol{\alpha}, \gamma, \boldsymbol{\beta}\right)=F\left(\mathbf{X}_{i t}^{\prime} \boldsymbol{\beta}+\alpha_{i}+\gamma_{t}\right)
$$

> 模型中有二元因变量 $Y_{i t} \in\{0,1\}$ 以及一个协变量向量 $\mathbf{X}_{i t}$，其中 $i$=1，…，N，时间 $t$=1。$i$ 与 $t$ 的定义适用于标准面板数据集。

> $F: \mathbb{R} \rightarrow[0,1]$ 是累积分布函数（Probit 模型中的标准正态分布以及 Logit 模型中的标准 logistic 分布）

> $\beta$ 是与 $\mathbf{X}_{i t}$ 维数相同的未知模型系数的向量

> 向量 $\mathbf{X}_{i t}$ 包含关于 $\mathbf{Y}_{i t}$ 的前定变量，特别地，$\mathbf{X}_{i t}$ 可以包含 $\mathbf{Y}_{i t}$ 的滞后以适应动态模型变化

该模型没有对协变量向量和未观测效应之间的关系施加任何限制。在经验应用中，对未观察到的效应的条件作用是控制内生性，因为个体效应和时间效应捕捉了可能与协变量相关的未观察到的异质性。

作者采取固定效应方法，将个体和时间效应作为待评估的参数，条件性观测值的对数似然函数为：

$$
\begin{aligned}
\ell_{i t}\left(\boldsymbol{\beta}, \alpha_{i}, \gamma_{t}\right):=Y_{i t} & \times \log \left\{F\left(\mathbf{X}_{i t}^{\prime} \boldsymbol{\beta}+\alpha_{i}+\gamma_{t}\right)\right\} \\
&+\left(1-Y_{i t}\right) \times \log \left\{1-F\left(\mathbf{X}_{i t}^{\prime} \boldsymbol{\beta}+\alpha_{i}+\gamma_{t}\right)\right\}
\end{aligned}
$$

> 其中 $\boldsymbol{\beta}^{0}$、$\boldsymbol{\alpha}^{0}$、$\boldsymbol{\gamma}^{0}$ 表示参数的真实值

$\beta$、$\alpha$、$\gamma$ 的固定效应模型估计量是通过最大化样本的对数似然函数得到的：

$$
(\widehat{\boldsymbol{\beta}}, \widehat{\boldsymbol{\alpha}}, \widehat{\gamma}) \in \operatorname{argmax}_{(\boldsymbol{\beta}, \boldsymbol{\alpha}, \gamma) \in \mathbb{R}^{\operatorname{dim} \beta+N+T}} \sum_{i, t} \ell_{i t}\left(\boldsymbol{\beta}, \alpha_{i}, \gamma_{t}\right)
$$

> 这是一个用于 Logit-Probit 模型的平滑凹最大化程序，然而因为对数似然函数不因变换而变化，所以存在完美的共线性问题，即对于任意 $c \in \mathbb{R}$，都有 $\alpha_{i} \mapsto \alpha_{i}+c$ 和 $\gamma_{t} \mapsto \gamma_{t}-c$。如果 $\mathbf{X}_{i t}$ 包含一个常数项，我们则通过去掉 $\alpha_{1}$ 和 $\gamma_{1}$ 来使得其值为 0。如果它不包含常数项，我们只需要去掉其中任意一个。
>
> 在线性面板模型中，除了常数项，协变量 $\mathbf{X}_{i t}$ 需要在 $i$ 和 $t$ 上变化，以避免进一步的完美共线性问题，也就是说，保证对数似然函数是严格凹的。

通过使用`logit`和`probit`命令，可以在「Stata」 中是实现上述的固定效应估计器，以说明 $\alpha_{i}$ 和 $\gamma_{t}$。但是 $\widehat{\boldsymbol{\beta}}$ 可能会有严重的偏差，现有的程序并不包含任何偏差校正方法。

在 Logit-Probit 模型应用中，最终的兴趣参数是协变量的**APE​**，其形式为：

$$
\boldsymbol{\delta}^{0}=\mathbb{E}\left\{\Delta\left(\boldsymbol{\beta}^{0}, \boldsymbol{\alpha}^{0}, \boldsymbol{\gamma}^{0}\right)\right\}, \quad \Delta(\boldsymbol{\beta}, \boldsymbol{\alpha}, \boldsymbol{\gamma})=(N T)^{-1} \sum_{i, t} \Delta\left(\mathbf{X}_{i t}, \boldsymbol{\beta}, \alpha_{i}, \gamma_{t}\right)
$$

> 其中 $\mathbb{E}\\$ 表示对联合分布数据和未观察到的影响预期

如果 $\mathbf{X}_{i t}$ 的元素是二进制的，那么它对于 $\mathbf{Y}_{i t}$ 条件概率的部分影响可以用该式子表示：

$$
\Delta\left(\mathbf{X}_{i t}, \boldsymbol{\beta}, \alpha_{i}, \gamma_{t}\right)=F\left(\beta_{k}+\mathbf{X}_{i t,-k}^{\prime} \boldsymbol{\beta}_{-k}+\alpha_{i}+\gamma_{t}\right)-F\left(\mathbf{X}_{i t,-k}^{\prime} \boldsymbol{\beta}_{-k}+\alpha_{i}+\gamma_{t}\right)
$$

如果不是二进制，则利用该式子计算使用：

$$
\Delta\left(\mathbf{X}_{i t}, \boldsymbol{\beta}, \alpha_{i}, \gamma_{t}\right)=\beta_{k} \partial F\left(\mathbf{X}_{i t}^{\prime} \boldsymbol{\beta}+\alpha_{i}+\gamma_{t}\right)
$$

APE 的固定效应估计量是通过上述样本模拟中插入模型参数估计量而获得的，所以：

$$
\widetilde{\boldsymbol{\delta}}=\Delta(\widetilde{\boldsymbol{\beta}}, \widetilde{\boldsymbol{\alpha}}, \widetilde{\gamma})
$$

$$
(\widetilde{\boldsymbol{\alpha}}, \widetilde{\boldsymbol{\gamma}}) \in \operatorname{argmax}_{(\boldsymbol{\alpha}, \gamma) \in \mathbb{R}^{N+T}} \sum_{i, t} \ell_{i t}\left(\widetilde{\boldsymbol{\beta}}, \alpha_{i}, \gamma_{t}\right)
$$

### 2.2 附带参数问题

固定效应模型的估计量 $\widehat{\boldsymbol{\beta}}$ 与 $\widetilde{\boldsymbol{\delta}}$ 都会遇到 Neyman 与 Scott 所提及的附带参数难题。尤其当模型具有个体效应之时，这些估计量在渐近序列下是不一致的。问题的根源在于，只有固定数量的观测值来估计每个未观测到的效应，每个单个效应的 $T$ 观测值或者每个时间效应下的 $N$ 观测值使得相应的估计值不一致。模型的非线性将个体或时间效应估计中的不一致性传播到所有模型系数和**APE**。

而最近对于附带参数问题的一个回应则是考虑一个替代的渐近近似，其中 $N$$\longrightarrow$$\infty$ 和 $T$$\longrightarrow$$\infty$。在这种近似下，附带参数问题则演变成为比传统渐近近似下的不一致问题更容易处理的偏差校正问题。

特别是 Fernández-Val 和 Weidner (2016)所表明，当 $N$，$T$$\longrightarrow$$\infty$，$N / T \rightarrow c>0$ 时，$\widehat{\boldsymbol{\beta}}$ 的极限分布描述如下：

$$
\sqrt{N T}\left(\widehat{\boldsymbol{\beta}}-\boldsymbol{\beta}^{0}-\mathbf{B}^{\beta} / T-\mathbf{D}^{\beta} / N\right) \rightarrow_{d} \mathcal{N}\left(\mathbf{0}, \mathbf{V}^{\beta}\right)
$$

> 其中 $\boldsymbol{V}^{\beta}$ 是渐近方差-协方差矩阵，$\boldsymbol{B}^{\beta}$ 是来自个体效应估计的渐近偏差项，$\boldsymbol{D}^{\beta}$ 是来自时间效应估计的渐近偏差项。

这个结果的有限样本预测是，即使 $N$ 和 $T$ 是同一数量级，固定效应模型估计量相对于它的离差也可能有显著的偏差。即使在大样本情况下，围绕固定效应模型估计量构建的置信区间也有可能严重掩盖参数的真实值。

而对于 $\widetilde{\boldsymbol{\delta}}$，由于标准差的阶数比 $\widehat{\boldsymbol{\beta}}$ 更慢，情况并不相同。在这种情况下，Fernández-Val 与 Weidner (2016)表示，极限分布为：

$$
\sqrt{\min (N, T)}\left(\widetilde{\boldsymbol{\delta}}-\boldsymbol{\delta}^{0}-\mathbf{B}^{\delta} / T-\mathbf{D}^{\delta} / N\right) \rightarrow_{d} \mathcal{N}\left(\mathbf{0}, \mathbf{V}^{\delta}\right)
$$

> $\boldsymbol{V}^{\delta}$ 是渐近方差，$\boldsymbol{B}^{\delta}$ 是来自个体效应估计的渐近偏差，$\boldsymbol{D}^{\delta}$ 是时间效应的估计渐近偏差项。在这里，标准差控制了两个偏置项，这意味着 $\widetilde{\boldsymbol{\delta}}$ 是渐近一阶无偏的。



### 2.3 分析偏差校正

分析偏差校正包括从 $\beta$ 的固定效应估计值中去除偏差的前导项估计值。让 $\widehat{\mathbf{B}}^{\beta}$ 与 $\widehat{\mathbf{D}}^{\beta}$ 变成 $\boldsymbol{B}^{\beta}$ 和 $\boldsymbol{D}^{\beta}$ 的一致估计量，偏差校正估计器可以写作：

$$
\widetilde{\boldsymbol{\beta}}^{A}=\widehat{\boldsymbol{\beta}}-\widehat{\mathbf{B}}^{\beta} / T-\widehat{\mathbf{D}}^{\beta} / N
$$

当 $N$，$T$$\longrightarrow$$\infty$，$N / T \rightarrow c>0$ 时，$\widetilde{\boldsymbol{\beta}}^{A}$ 的极限分布描述如下：

$$
\sqrt{N T}\left(\widetilde{\boldsymbol{\beta}}^{A}-\boldsymbol{\beta}^{0}\right) \rightarrow_{d} \mathcal{N}\left(\mathbf{0}, \mathbf{V}^{\beta}\right)
$$

因此，分析校正将渐近分布集中在参数的真实值上，而不增加渐近方差。该结果预测，在大样本中，校正后的估计量相对于离差具有较小的偏差，校正不会增加离差，并且围绕校正后的估计量构建的置信区间具有接近标称水平的覆盖概率。

偏差校正后的 APE 可以用与相同的方式构建：

$$
\widetilde{\boldsymbol{\delta}}^{A}=\widetilde{\boldsymbol{\delta}}-\widehat{\mathbf{B}}^{\delta} / T-\widehat{\mathbf{D}}^{\delta} / N
$$

让 $\widehat{\mathbf{B}}^{\delta}$ 与 $\widehat{\mathbf{D}}^{\delta}$ 变成 $\boldsymbol{B}^{\delta}$ 和 $\boldsymbol{D}^{\delta}$ 的一致估计量，极限分布可以表示为：

$$
\sqrt{\min (N, T)}\left\{\widetilde{\boldsymbol{\delta}}^{A}-\boldsymbol{\delta}^{0}+o_{P}\left(T^{-1}+N^{-1}\right)\right\} \rightarrow_{d} \mathcal{N}\left(\mathbf{0}, \mathbf{V}^{\delta}\right)
$$



### 2.4 Jackknife（刀切法）偏差校正

带有`jackknife`选项的`probitfe`和`logitfe`命令有六种不同类型的刀切法校正，分别为`ss1`、`ss2`、`js`、`sj`、`jj`和`double`。Jackknife 对于偏差的校正没有进行明确的估计，所以他们依靠减少偏差的直觉。但计算量更大，因为涉及到求解多个固定效应模型的估计程序。因篇幅有限，本文不再赘述。



### 2.5 单向固定效应模型

作者关注具有个体和时间效应的双向固定效应模型，因为他们是应用中最普遍的类型。`probitfe`和`logitfe`命令页提供了针对单向固定效应模型的功能，但仅包括个体效应或时间效应，以及选择偏差校正应仅考虑个体效应还是时间效应的灵活性。这些模型的固定效应估计量也有附带参数问题。



### 2.6 不平衡的面板数据

在附带参数问题和偏差修正的描述中，作者隐含地假设面板是平衡的。然而，不平衡的面板数据在是实践应用中很为常见。如果缺失观测值的来源是随机的，不平衡不会引入特殊的理论复杂性。



## 3.命令介绍

### 3.1 命令安装

```

ssc install probitfe, replace
ssc install logitfe, replace

```

### 3.2 命令语法

`probitfe`和`logitfe`拥有相同的语法，他们在使用中可以相互替代，在此我们以`probitfe`为例。

#### 3.2.1 未校正估计量

```probitfe depvar indepvars
probitfe depvar indepvars [if] [in], nocorrection [ieffects(string)
    teffects(string) population(integer)]
```

`depvar`：因变量

`indepvar`：解释变量

**options：**

**nocorrection** 计算 probit 固定效应模型估计值，但由于附带的参数问题，不校正偏差

> 如果指定了**nocorrection**的 option，但没有包含 effects 的类型，那么该模型将同时包含个体和时间效应。**ieffects** (_no_) 和**teffects** (_no_) 无法组合。

**ieffects** (_string_) 指定未校正估计量是否包含个体效应影响。

> **ieffects** (_yes_) 默认包含个体固定效应影响，**ieffects** (_no_) 则不包含。

**teffects** (_string_) 指定未校正估计量是否包含时间效应影响。

> **teffects**（_yes_）默认包含，**teffects**（_no_）则不包含。

**population **(_integer_) 通过有限总体校正(FPC)来调整 APE 的方差估计。

> 设 $m$ 为 probit 单位中包含的原始观测值的数量，$M$（大于等于 $m$）为用户申报的全部人群观测值的数量。APE 方差的计算则通过因子 $\mathrm{FPC}=(M-m) /(M-1)$ 进行修正。
>
> 需要注意的是，$M$ 引用的是观察总数，而非个体总数。例如，在 50 个时间段内跟踪了 10 个人，则必须设定为**population **(500) 。

#### 3.2.2 分析校正估计量

```
probitfe depvar indepvars [if] [in] [, analytical lags(integer)
   ieffects(string) teffects(string) ibias(string) tbias(string)
   population(integer)]
```

默认情况下，使用 Fernández-Val 和 Weidner (2016)得出的分析偏差校正来计算 probit 的固定效应估计值。

**options：**

**lags**(_integer_) 指定微调参数的值，以估计光谱期望值，默认值为**lags**(0)。当模型是静态的且有严格的外生回归时，使用该 option。

> 微调参数可以设置为 $0$ 到($T$-$1$)之间的任意值。当模型是动态的、某些回归因子是弱外生时，应使用大于 $0$ 的微调参数。一般不建议将微调参数的值设置为高于 $4$ 的值，除非 $T$ 非常大。

如果指定**analytical**的 option，但没有包含校正类型，那么该模型将同时包含对个体和时间影响的分析偏差校正。**ibias** (_no_) 和**tbias** (_no_) 无法组合。

**ibias**(_string_)指定分析校正是否考虑个体的影响。**ibias**(_yes_) 为默认值，用于校正来自单个固定效应的偏差。**ibias**(_no_)则省略了单个固定效应的分析偏差校正。

**tbias**(_string_)指定分析校正是否考虑时间效应。用法同上。

#### 3.2.3 刀切法（Jackknife）校正估计量

```probitfe depvar indepvars
probitefe depvar indepvars [if] [in], jackknife [ss1 [multiple(integer)
   individuals time] ss2 [multiple(integer) individuals time] js sj jj
   double ieffects(string) teffects(string) ibias(string) tbias(string)
   population(integer)]
```

同样地，默认情况下，使用 Fernández-Val 和 Weidner (2016)得出的刀切法（Jackknife）偏差校正来计算 probit 的固定效应估计值。

**options：**

**ss1 [multiple(integer) individuals time]** 在四个不重叠的子面板中指定 $SPJ$；在每个子面板中，有一半的个人和一半的时间段被忽略。

> **multiple**(_integer_)是一个 $ss1$ 的子选项，它允许不同的多分区，每个分区都是对面板中的观察值进行随机化。默认为**multiple**(_0_)；也就是说，分区是按照数据集中的原始顺序进行的。比如指定了
>
> **multiple**(_10_)，则 $ss1$ 估计量在面板上的 $10$ 个不同的随即观测值上计算 $10$ 次，得到的估计量是这 $10$ 次 $SPJ$ 校正的平均值。如果面板的某个维度没有自然观察顺序，则选用该 option。

> **individuals**指定仅在横截面尺寸上制作多个分区，**time**指定仅在时间维度上创建多个分区。如果两个 option 都没有被指定，则在横截面维度和时间维度上进行多个分区。

**ss2 [multiple(integer) individuals time]** 默认情况下，在两个维度上都指定 $SPJ$。和 $ss1$ 一样，有四个子面板；在其中两个子面板中，一半个体被遗漏，但是所有的时间段都包括在内；在另外两个子面板中，一半的时间周期被遗漏，但是所有的个体都包括在内。**multiple**(_integer_)是一个 $ss2$ 的子选项，其余均与 $ss1$ 指令用法相似。

**js**指在横截面中删除一个 $PJ$，在时间序列中选择使用 $SPJ$。

**sj**指在横截面上使用 $SPJ$，在时间序列中删除一个 $PJ$。

**jj**指在横截面和时间序列中使用删除一个 $PJ$。

**double**指的是对具有相同横截面和时间序列指数的观测值使用 delete-one 刀切。



## 4 Stata 实操：国家间的双边贸易流动

为了说明刀切法偏差校正和单项固定效应模型中描述的偏差校正使用，作者使用了来自 Helpman, Melitz 和 Rubinstein (2008)的数据对国家间双边贸易流动进行实证应用。该数据集包括了 158 个国家在 1970-1997 年期间的贸易流量，以及国家的地理、机构和文化数据。作者使用 Logit-Probit 模型计算了国家对之间的正贸易流动。数据结构是一个伪面板，其中两个维度包含国家，**id**为进口商，**jd**为出口商。建模如下：

$$
\operatorname{Pr}\left(\text { trade }_{i j}=1 \mid X_{i j}, \alpha_{i}, \gamma_{j}\right)=F\left(X_{i j}^{\prime} \beta+\alpha_{i}+\gamma_{j}\right)
$$

> 对于每个国家对，如果国家 $i$ 从国家 $j$ 进口，结果变量 $\text { trade }_{i j}$ 是等于 $1$ 的指标，否则为 $0$。
>
> $X_{i j}$ 为解释变量集，包含国家间距离、边界、法律、语言、种族、习俗、海岸线等多种因素。$\alpha_{i}$ 为未被观察到的进口商，$\gamma_{i}$ 为未被观察到的出口商。
>
> 尽管包含滞后因变量，但 $X_{i j}$ 被视为严格的外生变量，因为面板指数两个维度都没有时间。


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E5%BE%90%E7%90%AC%E8%90%B1_%E4%BA%8C%E5%85%83%E5%9B%BA%E5%AE%9A%E6%95%88%E5%BA%94%E6%A8%A1%E5%9E%8B_Fig04.png)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E5%BE%90%E7%90%AC%E8%90%B1_%E4%BA%8C%E5%85%83%E5%9B%BA%E5%AE%9A%E6%95%88%E5%BA%94%E6%A8%A1%E5%9E%8B_Fig03.png)

**表 1 和表 2 分别显示了 `logit` 模型和 `probit` 模型的结果。**

>在这两个表中，第(1)列报告未校正的 FEs 估计值，第(2)列报告将微调参数设置为零(AN-0)的分析校正估计值，第(3)至(5)列显示 ss2、jj 和 nd 双折刀校正的估计值。双重修正是有意义的，因为该小组的两个维度都索引了同一组国家。每个表都显示了指数系数和 APE 的估计值。
>
>第(6)列中指数系数的标准误差(SEs)和第(6)和第(7)列中的 SEs for the APE。在 APE 的情况下，第(7)栏中的总样本数由第 3.2 节中描述的 FPC 参数调整，使用的总样本数等于样本量(24，492)。只有一组 SEs，因为未校正估计量的 SEs 与校正估计量一致(见 Fernández-Val 和 Weidner [2016])。

>logit 模型的结果。从概率单位模型得出的结论是类似的，特别是在 APE 方面，与指数系数不同，APE 在不同模型之间具有可比性。如第(1)栏所示，如果 I 国在前一年已经从 j 国进口(ltrade)，如果这两个国家彼此更接近( ldist )，如果它们使用相同的语言(语言)，如果它们使用相同的货币(货币)，如果它们属于相同的区域自由贸易协定( fta )，如果它们不是岛屿，或者如果它们使用相同的宗教(宗教)，I 国从 j 国进口的概率更高。正如 Helpman、Melitz 和 Rubinstein (2008)所述，如果两国都有 comonlandborder(边境)，i 国从 j 国进口的可能性就会降低，他们将其归因于抑制邻国间贸易的领土边界冲突的影响。**不管使用哪种类型的校正，这些效果都是同一个方向。然而，不同的估计量所产生的影响的大小有一些差异。**

>跨列比较，可以看到 AN-0、JK-JJ 和 double 产生非常相似的指数系数和 APE 估计，它们都在彼此的一个 SE 内。第(3)列中 ldist、legal、currency 和 fta 的指数系数和 APE 的分割面板修正估计值与同一行中的其他估计值相差两个或更多 Se。相对于第(1)栏中的未修正估计值，贸易指数系数的修正估计值要低一个以上的标准差。作者将未校正和偏差校正估计值之间其余指数系数和 APE 的相似性部分归因于大样本量(JK-SS2 除外)。



为了评估偏差校正的性能，作者进行了模拟上述经验示例的蒙特卡罗模拟，作者发现 probit 模型有类似的结果。所有参数均根据上一节中使用的数据进行校准，其值设置为表 1 第(1)列中未校正的 固定效应模型估计值。为了加快计算速度，作者只考虑 $X_{i j}$中的两个解释变量：前一年的贸易( **ltrade** )和国家对之间的对数距离( **ldist** )。



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E5%BE%90%E7%90%AC%E8%90%B1_%E4%BA%8C%E5%85%83%E5%9B%BA%E5%AE%9A%E6%95%88%E5%BA%94%E6%A8%A1%E5%9E%8B_Fig01.png)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E5%BE%90%E7%90%AC%E8%90%B1_%E4%BA%8C%E5%85%83%E5%9B%BA%E5%AE%9A%E6%95%88%E5%BA%94%E6%A8%A1%E5%9E%8B_Fig02.png)

>表 3 报告了未校正估算器(FE)、将修剪参数设置为零的分析校正(AN-0)、刀切法校正 ss2 和刀切法校正双倍(double)的结果。作者从指数系数和 **APE** 的渐近分布的偏差和推断精度两个方面分析了这些估计量的性能。

>综上所述，表 3 显示，分析校正大大降低了未校正估计量的偏差，为所有考虑的样本大小产生了更精确的点和区间估计量。除了最小样本量 $N$ = 25 之外，折刀校正双倍执行类似于分析校正。分面板校正 $ss2$ 减少了偏差，但代价是增加了大多数样本大小的离差。
>
>在本应用中，$ss2$ 由所有样本大小的其他校正统一支配，以 RMSE 表示。这些结果与表 1 中的经验证据一致，表 1 中未修正的指数等级系数估计值比修正后的估计值低一个以上的标准差，除了 $ss2$ 外，未修正和修正后的 **APE** 估计值非常相似，刀切修正 $ss2$ 产生的 **ldist** 估计值与其他估计值不一致。



## 5 总结

`probitfe`和`logitfe`命令执行 Fernández-Val 和 Weidner (2016)对具有双向固定效应的 Logit-Probit 模型的分析和折刀偏差校正。这些命令计算指数系数和 APE 的估计量，它们通常是这些模型中感兴趣的参数。Logit-Probit 模型通常用于实证工作，这使得新命令成为应用计量经济学工具包中有价值的补充。



## 6 参考文献

- Andersen, E. B. 1973. Conditional Inference and Models for Measuring, vol. 5. Copen-
  hagen: Mentalhygiejnisk forlag.

- Anderson, J. E., and E. van Wincoop. 2003. Gravity with gravitas: A solution to the
  border puzzle. American Economic Review 93: 170–192.

- Arellano, M., and J. Hahn. 2007. Understanding bias in nonlinear panel models: Some
  recent developments. In Advances in Economics and Econometrics: Theory and
  Applications, ed. R. Blundell, W. K. Newey, and T. Persson, 381–409. Cambridge:
  Cambridge University Press.

- Chamberlain, G. 1984. Panel data. In Handbook of Econometrics, vol. 2, ed. Z. Griliches
  and M. D. Intriligator, 1247–1318. Amsterdam: Elsevier.

- Dhaene, G., and K. Jochmans. 2015. Split-panel jackknife estimation of fixed-effect
  models. Review of Economic Studies 82: 991–1030.

- Fernández-Val, I. 2009. Fixed effects estimation of structural parameters and marginal
  effects in panel probit models. Journal of Econometrics 150: 71–85.

- Fernández-Val, I., and M. Weidner. 2016. Individual and time effects in nonlinear panel
  models with large N, T. Journal of Econometrics 192: 291–312.

- Hahn, J., and W. Newey. 2004. Jackknife and analytical bias reduction for nonlinear
  panel models. Econometrica 72: 1295–1319.

- Helpman, E., M. Melitz, and Y. Rubinstein. 2008. Estimating Trade Flows: Trading
  Partners and Trading Volumes. Quarterly Journal of Economics 123: 441–487.

- Neyman, J., and E. L. Scott. 1948. Consistent estimates based on partially consistent
  observations. Econometrica 16: 1–32.

- Rasch, G. 1960. Probabilistic Models for Some Intelligence and Attainment Tests.
  Copenhagen: Danmarks Pædagogiske Institut.