*! version 15.0  2020
capture program drop eivprobit
program define eivprobit, eclass byable(onecall)  ///
		prop(or svyb svyj svyr)	
		
	if _caller() >= 11 {
		local vv : di "version " string(_caller()) ":"
	}
	
	
	if _by() {
		local BY `"by `_byvars'`_byrc0':"'
	}
	
	`BY' eivprobit11 `0'
	
	if replay() {
		if `"`e(cmdline)'"' != "eivprobit" { 
			error 301
		}
		else if _by() { 
			error 190 
		}
		else {
			Display `0'
		}
		exit
	}
	
	
	if "`s(exit)'" != "" {
		ereturn local cmdline `"eivprobit `0'"'
		exit
	}


end



capture program drop eivprobit11
program define eivprobit11, eclass byable(recall)
	local n 0

	
	gettoken lhs 0 : 0, parse(" ,[") match(paren)
	IsStop `lhs'
	if `s(stop)' == 1 { 
		error 198 
		}
	while `s(stop)'==0 { 
		if "`paren'"=="(" {
			local n = `n' + 1
			if `n'>1 { 
capture noi error 198 
di in red `"syntax is "(all instrumented variables = instrument variables)""'
exit 198
			}
			gettoken p lhs : lhs, parse(" =")
			while "`p'"!="=" {
				if "`p'"=="" {
capture noi error 198 
di in red `"syntax is "(all instrumented variables = instrument variables)""'
di in red `"the equal sign "=" is required"'
exit 198 
				}
				local end`n' `end`n'' `p'
				
				gettoken p lhs : lhs, parse(" =")
			}
			tsunab end`n' : `end`n''
			tsunab exog`n' : `lhs'
		}
		else {
			local exog `exog' `lhs'
		}
		gettoken lhs 0 : 0, parse(" ,[") match(paren)
		IsStop `lhs'
	}
	local 0 `"`lhs' `0'"'

	tsunab exog : `exog'
	tokenize `exog'
	local lhs "`1'"
	local 1 " " 
	local exog `*'
	
	syntax [anything(name=eqlist)][if] [in] [aw fw pw iw] [,         ///
		seed(integer 79846512)	                                     ///
		INTEract(varlist)	                                         ///
		QUAEffect(varlist)                                          ///
		MAEffect(varlist)                                            ///
		BOOTstrap(integer 50)                                        ///
	]
		
	
	if "`interact'" == ""{
	dis in red "you must enter an interact variable"
	erro 198
	}
	
	if "`quaeffect'" != ""{
		if "`maeffect'" != ""{
		dis in red  `"you could only enter quaeffect or maeffect !""'
		error 198
		}
	}
	
	marksample touse
	markout `touse' `lhs' `exog' `exog1' `end1'
	
	Subtract newexog : "`exog1'" "`exog'"
	
	_rmcoll `newexog'
	local newexog "`r(varlist)'"

	local endo_ct : word count `end1'
	local ex_ct : word count `newexog'
	
	if `endo_ct' > `ex_ct' {
		di in red "equation not identified; must have at " /*
		*/ "least as many instruments not in"
		di in red "the regression as there are "           /*
		*/ "instrumented variables"
		exit 481
	}	
	
	//list `exog'
	
	**lhs为被解释变量、end1为内生变量、exog为其他外生变量，不包括工具变量
	**exog1为工具变量
	cap drop x1x2
	gen x1x2 = `interact'*`end1' 
	**lhs为被解释变量、end1为内生变量、exog为其他外生变量，不包括工具变量
	**exog1为工具变量
	cap drop y x1 x2 control ivs4

	local y `lhs'
	local x1 `end1'
	local x2 `interact'
	local control `exog'
	local ivs4 `exog1'	
	
	if "`quaeffect'" == ""{
	**first-stage:
	cap drop v1hat x1hat
	qui reg `x1' `ivs4' `x2' `control' if `touse'
	predict v1hat if `touse',resid 
	predict x1hat if `touse'

	***second-stage:
	cap drop yhat v a1_b
	qui probit `y' `x1' `x2' x1x2 `control' v1hat if `touse'
	predict yhat 
	gen v=_b[v1hat]*v1hat 
	gen a1_b=yhat-v    //a1*x1+a2*x2+a3*x1*x2+x'b

	drop if `y'==. | `x1'==. | `x2'==.
	drop if a1_b==. | v==.
	}
	
	if "`quaeffect'" != ""{
	cap drop qua2 
	gen qua2 = `quaeffect'*`quaeffect' if `touse'
	**first-stage:
	cap drop v1hat x1hat
	qui reg `x1' `ivs4' `x2' qua2 `control'  if `touse'
	predict v1hat if `touse',resid 
	predict x1hat if `touse'
	***second-stage:
	cap drop yhat v a1_b
	qui probit `y' `x1' `x2' x1x2 qua2 `control' v1hat if `touse'
	predict yhat ,xb
	gen v=_b[v1hat]*v1hat 
	gen a1_b=yhat-v    //a1*x1+a2*x2+a3*x1*x2+x'b
	drop if `y'==. | `x1'==. | `x2'==.
	drop if a1_b==. | v==.
	}
	
	cap drop ave1 ave2 ave3
	gen ave1=0 
	gen ave2=0
	gen ave3=0

	local nn=_N
	**导入superX，输入为a1_b,v。计算ave1，ave2，ave3
	cap drop Variable1 Variable2
	SuperX a1_b v,t(Normal)
	**normal形式的SuperX
	replace ave1=Variable1
	**normalden形式的SuperX
	cap drop Variable1 Variable2
	SuperX a1_b v,t(Normalden)
	replace ave2=Variable1
	replace ave3=Variable2
	cap drop Variable1 Variable2
	
	cap drop mex1
	gen mex1=(_b[`x1']+_b[x1x2]*`x2')*ave2 if `touse'
	cap drop mex2 
	gen mex2=(_b[`x2']+_b[x1x2]*`x1')*ave2 if `touse'
	cap drop inteff 
	gen inteff=_b[x1x2]*ave2-(_b[`x2']+_b[x1x2]*`x1')*(_b[`x1']+_b[x1x2]*`x2')*ave3 if `touse' 
	
	if "`maeffect'" != ""{
	//interaction effect
	cap drop meage 
	//检验要计算的边际效应项是否在控制变量中
	cap drop test_a 
	scalar test_a = -1000
	cap scalar test_a = _b[`maeffect']
	if test_a == -1000{
		dis in red "you should enter the maeffect variable into the control variables"
		error 198
		}
		
	gen meage=_b[`maeffect']*ave2 if `touse'         
	//marginal propability effect of age
	}
	
	if "`quaeffect'" != ""{
	//meaffect and quadratic effect
	cap drop test_a 
	scalar test_a = -1000
	cap scalar test_a = _b[`quaeffect']
	if test_a == -1000{
		dis in red "you should enter the maeffect variable into the control variables"
		error 198
		}
	
	cap drop meage 
	gen meage=(_b[`quaeffect']+2*_b[qua2]*`quaeffect')*ave2  if `touse' 
	cap drop qeage
	gen qeage=2*_b[qua2]*ave2-(_b[`quaeffect']+2*_b[qua2]*`quaeffect')^2*ave3   if `touse'   
	}

	**calculate the average effects:mex1_m mex2_m inteff_m meage_m qeage_m
	qui{
	sum mex1 
	scalar mex1_m=r(mean)    //mean marginal effect of x1
	sum mex2 
	scalar mex2_m=r(mean)      //mean marginal effect of x2
	sum inteff 
	scalar inteff_m=r(mean)    //mean interaction effects of x1,x2
	
	if "`maeffect'" != ""{
	sum meage 
	scalar meage_m=r(mean)   //mean marginal effect of x3
	}
	
	if "`quaeffect'" != ""{
	sum meage 
	scalar meage_m=r(mean)   //mean marginal effect of x3
	sum qeage 
	scalar qeage_m=r(mean)   //mean quadratic effect of x3
	}
	
}

	cap sum mex1 mex2 inteff meage qeage

	***parametric bootstrap by residuals based on the two-step estimation
	cap drop mex1_b
	gen mex1_b=0
	cap drop mex2_b
	gen mex2_b=0
	cap drop inteff_b 
	gen inteff_b=0
	if "`maeffect'" != ""{
	cap drop meage_b
	gen meage_b=0
	}
	
	if "`quaeffect'" != ""{
	cap drop meage_b
	gen meage_b=0
	cap drop qeage_b
	gen qeage_b=0
	}


	set seed `seed'
	cap drop nB
	scalar nB=_N
	if nB >= `bootstrap'{
	local B `bootstrap'    //bootstrap replicates
}
	if nB < `bootstrap'{
	local B nB    //bootstrap replicates
}
	
	local nn=_N  //sample size

	forvalues i=1(1)`B'{
	dis "run=" `i'
	
	if "`quaeffect'" == ""{
	***first-stage
	cap drop v1hat x1hat
	qui reg `x1' `ivs4' `x2' `control' if `touse' 
	predict v1hat if `touse',resid
	predict x1hat if `touse',xb
	***second-stage:
	cap drop yhat 
	qui probit `y' `x1' `x2' x1x2 `control' v1hat if `touse'
	predict yhat if `touse', xb
	}
	
	if "`quaeffect'" != ""{
	***first-stage
	cap drop v1hat x1hat
	qui reg `x1' `ivs4' `x2' qua2 `control' if `touse' 
	predict v1hat if `touse',resid
	predict x1hat if `touse',xb
	***second-stage:
	cap drop yhat 
	qui probit `y' `x1' `x2' x1x2 qua2 `control' v1hat if `touse'
	predict yhat if `touse', xb
	}

	cap drop a1_b
	gen a1_b=yhat-_b[v1hat]*v1hat    //a1*x1+a2*x2+a3*x1*x2+x'b
	
	cap drop v1s 
	gen v1s=v1hat[ceil(uniform()*`nn')] //resample the residuals v1f with replacement
	cap drop x1s
	gen x1s=x1hat+v1s        //generate the boostrap series of x1
	cap drop x1sx2 
	gen x1sx2=x1s*`x2'       if `touse'
	cap drop ave1s
	gen ave1s=0              
	 
	**导入superX，输入为_b[$x1]*x1s+_b[$x2]*$x2+_b[$x1x2]*x1sx2+(a1_b-(_b[$x1]*$x1+_b[$x2]*$x2+_b[$x1x2]*$x1x2)),_b[v1hat]*v1s。计算ave1s
	cap drop a1_bb
    gen a1_bb=_b[`x1']*x1s+_b[`x2']*`x2'+_b[x1x2]*x1sx2+(a1_b-(_b[`x1']*`x1'+_b[`x2']*`x2'+_b[x1x2]*x1x2))      if `touse'
	cap drop bv1s
	gen bv1s = _b[v1hat]*v1s
	
	
	SuperX a1_bb bv1s,t(Normal)
	**normal形式的SuperX
	replace ave1s=Variable1
	cap drop Variable1 Variable2
	
	cap drop ys
	gen U=uniform()
	qui gen ys=1 if U<=ave1s
	qui replace ys=0 if U>ave1s     //generate bootstrap y*

	cap drop U ave1s v1s 

	***use boostrap sample {ys,x1s,x2,x1sx2,x,$ivs4} to estimate 
	cap drop v1ss
	
	if "`quaeffect'" == ""{
	***first-stage:
	qui reg x1s `ivs4' `x2' `control'  if `touse'
	predict v1ss if `touse',resid
	***second-stage:
	cap drop xbs
	qui probit ys x1s `x2' x1sx2 `control' v1ss  if `touse'
	predict xbs if `touse', xb	
	}
	
	if "`quaeffect'" != ""{
	***first-stage:
	qui reg x1s `ivs4' `x2' qua2 `control'  if `touse'
	predict v1ss if `touse',resid
	***second-stage:
	cap drop xbs
	qui probit ys x1s `x2' x1sx2 qua2 `control' v1ss  if `touse'
	predict xbs if `touse', xb	
	}
	
	
	cap drop vs 
	gen vs=_b[v1ss]*v1ss 
	cap drop a1_bs 
	gen a1_bs=xbs-vs   //a1*x1+a2*x2+a3*x1*x2+x'b
	cap drop ave2s
	gen ave2s=0
	cap drop ave3s
	gen ave3s=0


	**导入superX，输入为a1_bs和vs。计算ave2，ave3*********************************
	SuperX a1_bs vs,t(Normalden)
	**normal形式的SuperX
	replace ave2s=Variable1
	replace ave3s=Variable2
	drop Variable1 Variable2

	cap drop mex1s 
	gen mex1s=(_b[x1s]+_b[x1sx2]*`x2')*ave2s      if `touse'          
	//marginal effects of x1 in observations
	cap drop mex2s
	gen mex2s=(_b[`x2']+_b[x1sx2]*x1s)*ave2s      if `touse'             
	//marginal effects of x2 in observations
	cap drop inteffs
	gen inteffs=_b[x1sx2]*ave2s-(_b[`x2']+_b[x1sx2]*x1s)*(_b[x1s]+_b[x1sx2]*`x2')*ave3s if `touse'   	//interaction effects of x1,x2 in observations
	
	if "`maeffect'" != ""{
	//marginal effects of age in observations
	cap drop meages
	gen meages=_b[`maeffect']*ave2s      if `touse'  
	}
	
	if "`quaeffect'" != ""{
	//marginal effects of age in observations
	cap drop meages
	gen meages=(_b[`quaeffect']+2*_b[qua2]*`quaeffect')*ave2s   if `touse'  
	//quadratic effects of age in observations	
	cap drop qeages
	gen qeages=2*_b[qua2]*ave2s-(_b[`quaeffect']+2*_b[qua2]*`quaeffect')^2*ave3s   if `touse'  
	}
	

	qui{
	sum mex1s
	replace mex1_b=r(mean) in `i'
	sum mex2s
	replace mex2_b=r(mean) in `i'
	sum inteffs
	replace inteff_b=r(mean) in `i'
	cap sum meages
	cap replace meage_b=r(mean)  in `i'
	cap sum qeages
	cap replace qeage_b=r(mean) in `i'
		}
	cap drop mex1s mex2s inteffs meages qeages ave2s ave3s a1_bs vs xbs v1ss ys x1sx2 x1s v1s
	}

	** now obtain the bootstraped sample of effects 
	**{mex1_b mex2_b inteff_b meage_b qeage_b, b=1,2,...,B}
	replace mex1_b = . if mex1_b==0
	replace mex2_b = . if mex2_b==0
	replace inteff_b = . if inteff_b==0
	
	if "`maeffect'" != ""{
	replace meage_b = . if meage_b==0
	}
	
	if "`quaeffect'" != ""{
	replace meage_b = . if meage_b==0
	replace qeage_b = . if qeage_b==0
	}


	**calculate bootstrapped se, z, pv, CI of mex1_m mex2_m inteff_m meage_m qeage_m
	local effects mex1 mex2 inteff 
	foreach A of local effects {
	qui sum `A'_b  
	scalar `A'_bse=r(sd)  //bootstrap se
	scalar `A'_bz=`A'_m/`A'_bse  //bootstrap z
	scalar `A'_pv= 2*(1-normal(abs(`A'_bz)))   //bootstrap p-value based on normality
	scalar `A'_l=`A'_m-1.96*`A'_bse
	scalar `A'_r=`A'_m+1.96*`A'_bse  //bootstrapped CI based on normality
	}


	if "`maeffect'" != ""{
	qui sum meage_b
	scalar meage_bse=r(sd)  //bootstrap se
	scalar meage_bz=meage_m/meage_bse  //bootstrap z
	scalar meage_pv= 2*(1-normal(abs(meage_bz)))   //bootstrap p-value based on normality
	scalar meage_l=meage_m-1.96*meage_bse
	scalar meage_r=meage_m+1.96*meage_bse  //bootstrapped CI based on normality
	}
	
	
	if "`quaeffect'" != ""{
	local effects meage qeage
	foreach A of local effects {
	qui sum `A'_b  
	scalar `A'_bse=r(sd)  //bootstrap se
	scalar `A'_bz=`A'_m/`A'_bse  //bootstrap z
	scalar `A'_pv= 2*(1-normal(abs(`A'_bz)))   //bootstrap p-value based on normality
	scalar `A'_l=`A'_m-1.96*`A'_bse
	scalar `A'_r=`A'_m+1.96*`A'_bse  //bootstrapped CI based on normality
	}
	}


	if "`maeffect'" == ""{
		if "`quaeffect'" == ""{
***display in a table
dis  "          mean        se           z           pv          confidence_interval"    _newline  ///
     " mex1:   " %6.4f mex1_m  "     "  %6.4f mex1_bse  "      "  %6.4f mex1_bz  "       " %6.4f mex1_pv "       " %6.4f mex1_l "       "   %6.4f mex1_r     _newline  ///
     " mex2:   " %6.4f mex2_m  "     "  %6.4f mex2_bse  "      "  %6.4f mex2_bz  "       " %6.4f mex2_pv "        " %6.4f mex2_l "       "   %6.4f mex2_r    _newline  ///	   
     " inteff: " %6.4f inteff_m  "     "  %6.4f inteff_bse  "      "  %6.4f inteff_bz  "       " %6.4f inteff_pv "       " %6.4f inteff_l "       "   %6.4f inteff_r        _newline  
	}
}



	if "`maeffect'" != ""{
	
***display in a table
dis  "          mean        se           z           pv          confidence_interval"    _newline  ///
     " mex1:   " %6.4f mex1_m  "     "  %6.4f mex1_bse  "      "  %6.4f mex1_bz  "       " %6.4f mex1_pv "       " %6.4f mex1_l "       "   %6.4f mex1_r     _newline  ///
     " mex2:   " %6.4f mex2_m  "     "  %6.4f mex2_bse  "      "  %6.4f mex2_bz  "       " %6.4f mex2_pv "        " %6.4f mex2_l "       "   %6.4f mex2_r    _newline  ///	   
     " inteff: " %6.4f inteff_m  "     "  %6.4f inteff_bse  "      "  %6.4f inteff_bz  "       " %6.4f inteff_pv "       " %6.4f inteff_l "       "   %6.4f inteff_r        _newline  ///
     " meage:  " %6.4f meage_m  "     "  %6.4f meage_bse  "      "  %6.4f meage_bz  "       " %6.4f meage_pv  "        " %6.4f meage_l "       "   %6.4f meage_r  _newline  
	}
	

	
	if "`quaeffect'" != ""{
***display in a table
dis  "          mean        se           z           pv          confidence_interval"    _newline  ///
     " mex1:   " %6.4f mex1_m  "     "  %6.4f mex1_bse  "      "  %6.4f mex1_bz  "       " %6.4f mex1_pv "       " %6.4f mex1_l "       "   %6.4f mex1_r     _newline  ///
     " mex2:   " %6.4f mex2_m  "     "  %6.4f mex2_bse  "      "  %6.4f mex2_bz  "       " %6.4f mex2_pv "        " %6.4f mex2_l "       "   %6.4f mex2_r    _newline  ///	   
     " inteff: " %6.4f inteff_m  "     "  %6.4f inteff_bse  "      "  %6.4f inteff_bz  "       " %6.4f inteff_pv "       " %6.4f inteff_l "       "   %6.4f inteff_r        _newline  ///
     " meage:  " %6.4f meage_m  "     "  %6.4f meage_bse  "      "  %6.4f meage_bz  "       " %6.4f meage_pv  "        " %6.4f meage_l "       "   %6.4f meage_r  _newline  ///
     " qeage: " %6.4f qeage_m  "     "  %6.4f qeage_bse  "     "  %6.4f qeage_bz  "       " %6.4f qeage_pv "       " %6.4f qeage_l "      "   %6.4f qeage_r _newline 
	}
	
	ereturn local marginsok "XB default"
	ereturn hidden local marginsprop minus 
	ereturn local title "effec probit with endogenous variable"

	if "`end1'" != "" {
		ereturn local insts `exog' `newexog'
	}
	ereturn local cmdline "eivprobit"
	_prefix_footnote
	
	
end


//Display program using modified header
capture program drop Display
program Display
        syntax, [*]
	_prefix_display, `options'
end

capture program drop IsStop
program define IsStop, sclass
				/* sic, must do tests one-at-a-time, 
				 * 0, may be very large */
	if `"`0'"' == "[" {		
		sret local stop 1
		exit
	}
	if `"`0'"' == "," {
		sret local stop 1
		exit
	}
	if `"`0'"' == "if" {
		sret local stop 1
		exit
	}
	if bsubstr(`"`0'"',1,3) == "if(" {
		sret local stop 1
		exit
	}
	if `"`0'"' == "in" {
		sret local stop 1
		exit
	}
	if `"`0'"' == "" {
		sret local stop 1
		exit
	}
	else	sret local stop 0
end

capture program drop Subtract
program define Subtract   /* <cleaned> : <full> <dirt> */
	args	    cleaned     /*  macro name to hold cleaned list
		*/  colon	/*  ":"
		*/  full	/*  list to be cleaned 
		*/  dirt	/*  tokens to be cleaned from full */
	
	tokenize `dirt'
	local i 1
	while "``i''" != "" {
		local full : subinstr local full "``i''" "", word all
		local i = `i' + 1
	}

	tokenize `full'			/* cleans up extra spaces */
	c_local `cleaned' `*'       
end

cap program drop SuperX
  program define SuperX
  version 15.0
    syntax [varlist(min=2 max=2 numeric)][if][in][,t(string)]
        mata: block_operation("`varlist'","`t'")
        svmat Variable, names(Variable)
end

capture mata mata drop block_operation()
version 15.0
mata:
void block_operation(string rowvector varlist,string scalar t)
{
       real matrix D,V,XV,NormalXV,Y,XV_part,NormalXV_part,Y_part,Variable
       real colvector x,v,V1,V2,x_part
       real scalar n, k, m
       st_view(D,.,tokens(varlist))
       x=D[.,1]
       v=D[.,2]
	   n=rows(x)
	   if(t=="Normal"){
	      if(n<=5000){
		   //V=mm_expand(v',n)
		   V=v':*J(n,n,1)
           //需要提前安装moremata
           //先将v转置成行向量，再利用moremata里的mm_expand函数将其复制n行，即为n*n矩阵
           XV=x:+V
           //将列向量x中的元素逐列加到V的每一列，得n*n的XV矩阵
           NormalXV=normal(XV)
           //对XV矩阵每个元素求正态累计分布，得到矩阵NormalXV
           Y=NormalXV:*XV
           //矩阵XV与NormalXV点乘
           V1=(rowsum(NormalXV))/n
           //求得目标变量V1
           V2=(rowsum(Y))/n
           //求得目标变量V2
		  }
		  else{
             k=floor(10000000/n)
             //设置每次分块的行数,k的大小与个人电脑的配置有关，这里如何根据个人电脑来确定k是个需要考虑的问题。
             //k越大，运行时间越短
             m=ceil(n/k)
             //求出分块矩阵的数量
             //V=mm_expand(v',k)
			 V=v':*J(k,n,1)
             //先将v转置成行向量，再利用moremata（需要提前安装）里的mm_expand函数将其复制k行，即为k*n矩阵
             V1=J(n,1,.)
             V2=J(n,1,.)
             //V1、V2为目标变量，先设置目标变量为空矩阵
             //对前m-1块“分块矩阵”进行运算，用循环来变量每个分块矩阵
             for (i=1; i<=m-1; i++) {
             x_part=x[((k*(i-1)+1)::(k*i)),.]
             XV_part=x_part:+V
             //将列向量x_part中的元素逐列加到V的每一列，得k*n的XV矩阵
             NormalXV_part=normal(XV_part)
             //对XV_part矩阵每个元素求正态累计分布，得到矩阵NormalXV_part
             Y_part=NormalXV_part:*XV_part
             //矩阵XV_part与NormalXV_part点乘
             V1[(k*(i-1)+1)::(k*i),1]=(rowsum(NormalXV_part))/n
             V2[(k*(i-1)+1)::(k*i),1]=(rowsum(Y_part))/n
             }
             //对第m块“分块矩阵”进行运算
             x_part=x[((k*(m-1)+1)::n),.]
             XV_part=x_part:+(v':*J(n-k*(m-1),n,1))
             //将列向量x_part中的元素逐列加到V的每一列，得k*n的XV矩阵
             //NormalXV_part=normal(XV_part)
		     NormalXV_part=normal(XV_part)
             //对XV_part矩阵每个元素求正态累计分布，得到矩阵NormalXV_part
             Y_part=NormalXV_part:*XV_part
             //矩阵XV_part与NormalXV_part点乘
             V1[(k*(m-1)+1)::n,1]=(rowsum(NormalXV_part))/n
             V2[(k*(m-1)+1)::n,1]=(rowsum(Y_part))/n
			 }
		}
		else{
		   if(n<=5000){
		   V=v':*J(n,n,1)
           //需要提前安装moremata
           //先将v转置成行向量，再利用moremata里的mm_expand函数将其复制n行，即为n*n矩阵
           XV=x:+V
           //将列向量x中的元素逐列加到V的每一列，得n*n的XV矩阵
           NormalXV=normalden(XV)
           //对XV矩阵每个元素求正态累计分布，得到矩阵NormalXV
           Y=NormalXV:*XV
           //矩阵XV与NormalXV点乘
           V1=(rowsum(NormalXV))/n
           //求得目标变量V1
           V2=(rowsum(Y))/n
           //求得目标变量V2
		  }
		  else{
             k=floor(10000000/n)
             //设置每次分块的行数,k的大小与个人电脑的配置有关，这里如何根据个人电脑来确定k是个需要考虑的问题。
             //k越大，运行时间越短
             m=ceil(n/k)
             //求出分块矩阵的数量
             //V=mm_expand(v',k)
			 V=v':*J(k,n,1)
             //先将v转置成行向量，再利用moremata（需要提前安装）里的mm_expand函数将其复制k行，即为k*n矩阵
             V1=J(n,1,.)
             V2=J(n,1,.)
             //V1、V2为目标变量，先设置目标变量为空矩阵
             //对前m-1块“分块矩阵”进行运算，用循环来变量每个分块矩阵
             for (i=1; i<=m-1; i++) {
             x_part=x[((k*(i-1)+1)::(k*i)),.]
             XV_part=x_part:+V
             //将列向量x_part中的元素逐列加到V的每一列，得k*n的XV矩阵
             NormalXV_part=normalden(XV_part)
             //对XV_part矩阵每个元素求正态累计分布，得到矩阵NormalXV_part
             Y_part=NormalXV_part:*XV_part
             //矩阵XV_part与NormalXV_part点乘
             V1[(k*(i-1)+1)::(k*i),1]=(rowsum(NormalXV_part))/n
             V2[(k*(i-1)+1)::(k*i),1]=(rowsum(Y_part))/n
             }
             //对第m块“分块矩阵”进行运算
             x_part=x[((k*(m-1)+1)::n),.]
             XV_part=x_part:+(v':*J(n-k*(m-1),n,1))
             //将列向量x_part中的元素逐列加到V的每一列，得k*n的XV矩阵
             //NormalXV_part=normal(XV_part)
		     NormalXV_part=normalden(XV_part)
             //对XV_part矩阵每个元素求正态累计分布，得到矩阵NormalXV_part
             Y_part=NormalXV_part:*XV_part
             //矩阵XV_part与NormalXV_part点乘
             V1[(k*(m-1)+1)::n,1]=(rowsum(NormalXV_part))/n
             V2[(k*(m-1)+1)::n,1]=(rowsum(Y_part))/n
			 }
		}
			 Variable=(V1,V2)
	         st_matrix("Variable", Variable)
}
end

exit
