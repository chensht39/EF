# Stata机器学习分类器：c_ml_stata

## 1. 引言

*（该部分作为引言主要介绍**机器学习算法及分类问题**的相关内容）*

1. 简要介绍机器学习（What is machine learning, ML），阐述机器学习在经济、金融等学科的应用；
2. 介绍传统机器学习中的两大问题——分类问题和回归问题，并简单介绍其应用。

## 2. 机器学习的分类算法简介

*简要介绍c_ml_stata中的分类算法，由于该算法种类较多，且相互之间的联系不高，所以可以分开介绍：*

1. 经典的分类算法：
   1. 支持向量机SVM：比较经典的机器学习分类算法；
   2. 最近邻算法KNN：也是比较经典的无监督学习分类算法；
   3. 朴素贝叶斯：与SVM不同，朴素贝叶斯属于生成算法，也是比较经典的分类算法。
2. 现在比较流行的分类算法：
   1. 树类模型：决策树、提升算法、随机森林可以放一起介绍；
   2. 神经网络

*我不太熟悉“正则化多项式”，希望老师能给一些指导*。

**由于这些算法比较多，相互之间的联系也并非那么密切，所以可以分开介绍——分为传统算法和流行算法**，看看老师如何安排。同时，感觉还是要介绍一下算法的基本原理，但可能这并不是重点。

## 3. c_ml_stata命令简介

` c_ml_stata `是**Stata 16**中用于实现**机器学习分类算法（classification algorithms）**的命令。它使用Stata/Python集成（SFI）能力的Stata 16，并允许实现以下分类算法：

- 决策树（tree model）
- 提升算法（boosting，集成学习）
- 随机森林（random  forest）
- 正则化多项式（regularized multinomial，*感觉像是带惩罚项的回归*）
- 神经网络（neural network）
- 朴素贝叶斯（naive Bayes）
- 最近邻算法（nearest neighbor，*应该是KNN*）
- 支持向量机（support vector machine, SVM）

`c_ml_stata`还支持通过贪婪搜索的K-折交叉验证（K-fold cross-validation）提供超参数的最优调整。

对于每个观测值（或实例），该命令使用贝叶斯分类规则生成预测的类概率和预测的标签。该命令使用Python Scikit-learn API来执行交叉验证和预测。

## 4. 命令的安装

输入如下命令，查看完整程序文件和相关附件。

```stata
· net describe c_ml_stata
```

*（需要提前安装python的scikit-learn库等依赖库）*

## 5. Stata实操

- 这里作为示例用该命令提供的数据集及例子进行实操

```stata
use "c_ml_stata_data_example.dta"

h c_ml_stata

c_ml_stata y x1-x4 , mlmodel(tree) in_prediction("in_pred") cross_validation("CV") ///
out_sample("c_ml_stata_data_new_example") out_prediction("out_pred") seed(10) save_graph_cv("graph_cv")
```

作者提供的数据集比较简单，但是比较遗憾的是作者未提供样本外测试集的真实结果，所以不太能评定分类的效果到底如何。也可以考虑使用其他经典的分类数据集。

## 6. 参考文献

略



**最后，我看到除了`c_ml_stata`命令外，还有一个使用机器学习做回归问题的命令`r_ml_stata`，如果老师想一起配套介绍这两个命令我觉得也可以尝试（因为我看到`r_ml_stata`的模型基本上和`c_ml_stata`命令相似，其实回归问题和分类问题在某种程度上是紧密联系的）。**