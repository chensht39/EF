&emsp;

> **作者：** 彭礼敏 (中山大学)        
> **E-mail:**  <penglm3@mail2.sysu.edu.cn>   

&emsp;

---
# Fama–MacBeth (1973) 方法
**目录**
---


## 1.简介

### 1.1. 方法概述: (这里参考了 Cochrane 2015 (235-246)，并且假定$\beta$不随时间改变)
考虑一个K因子定价模型，
$$ E(R^{ei}) = \beta_{i}' \lambda \qquad i=1,2,...,N.\tag{1}$$
$R^{ei}$表示资产$i$的风险溢价，$\lambda$是$k$维因子溢价向量，$\beta_{i}$是资产$i$在$\lambda$上的$k$维因子暴露向量。模型$(1)$表示了超额收益和$\beta$的线性关系。

对于模型$(1)$，Fama-MacBeth方法分两步进行估计——先对各个资产$i$进行时间序列回归估计$\beta_i$,再对各个时期分别做截面回归估计。

- 第一步：对各个资产$i$分别做时间序列回归:
$$ R^{ei}_{t} = a_{i} + \beta_{i}'f_{t}+\varepsilon^{i}_{t},\qquad t=1,2,...,T.\quad  \tag{2}$$

- 第二步：用第一步得到的$\hat{\beta_{i}}$作为自变量，对各个时期$t$分别做截面回归
$$ R^{ei}_{t} = \hat\beta_{i}'\lambda_{t}+\alpha_{it},\qquad i=1,2,...,N. \quad \tag{3}$$

最后，对估计结果$\hat\lambda_{t}$, $\hat\alpha_{it}$在时序上取平均：
$$\hat\lambda = \frac{1}{T}\sum_{t=1}^{T}\hat\lambda_{t}, \quad \hat\alpha_{i} = \frac{1}{T}\sum_{t=1}^{T}\hat\alpha_{it}.$$
得到对$\lambda$，$\alpha$的估计。


#### 1.1.1. 相比传统截面回归的优势
传统截面回归和Fama MacBeth回归都是两步回归，第一阶段通过时序回归估计$\beta_i$，作为第二阶段的自变量。它们的区别就体现在在第二阶段，传统截面回归在时序回归得到$\beta_i$后，在第二阶段直接以超额收益率的在时序上的均值$E_{T}(R^{ei})$作为因变量，以第一阶段时序回归得到的$\beta_{i}$作为自变量，估计下式：
$$ E_{T}(R^{ei}) = \beta_{i}' \lambda + \alpha_{i},\qquad i=1,2,...,N.$$
其中$\lambda$因子风险溢价是待估参数，残差项$\alpha_{i}$是定价误差。

优势1： 修正了面板数据在截面上的相关性

优势2： 第二部截面回归允许使用时变的$\beta$做自变量

#### 1.1.2. 不足和修正
不足1： 没有解决变量误差(Error in Variable,EIV)的问题

不足2： 没有解决残差序列时序上相关的问题
### 1.2. 应用情况
#### 1.2.1. 检验CAPM：(这里介绍滚动窗口和时变$\beta$)
Fama and MacBeth(1973，jpe)
#### 1.2.2. 检验因子定价能力：
 Fama & French [The Cross-Section of Expected Stock Returns](https://onlinelibrary.wiley.com/doi/epdf/10.1111/j.1540-6261.1992.tb04398.x) (JF, 1992) 
通过FM回归发现，公司规模(size)和账面市值(book-to-market equity)可以解释股票平均收益截面上的变化。

[Size and value in China](https://www.sciencedirect.com/science/article/pii/S0304405X19300625)(JFE, 2019) 通过FM回归验证中国版三因子模型的定价能力

## 2. 理论基础和模型设定：(数学的部分还没整理好)
### 2.1. 模型设定(这里把1再详细一点儿)

$$ R^{ei}_{t} = a_{i} + \beta_{i}'f_{t}+\varepsilon^{i}_{t},\qquad t=1,2,...,T.\quad  \tag{2}$$
$$ R^{ei}_{t} = \hat\beta_{i}'\lambda_{t}+\alpha_{it},\qquad i=1,2,...,N. \quad \tag{3}$$

(这里还不太对，需要翻下书，，，还有角标的问题)
$\hat{\beta}$ 一致性的要求：
$$corr(\varepsilon^{i}_{t},f_{t}) = 0, \forall i; $$
$\hat{\beta}$ 有效性的要求：
$$\varepsilon_{t}\quad i.i.d. (关于t \quad i.i.d) ; $$
$\hat{\lambda_{t}}$ 一致性的要求：
$$corr(\alpha_{it},\lambda_{t}) = 0, \forall t; $$
$\hat{\lambda_{t}}$ 有效性的要求：
$$\alpha_{it} \quad i.i.d. (关于i \quad i.i.d) ; $$
$\hat\lambda$有效性的要求：
$$\lambda_{t} \quad i.i.d. (关于t \quad i.i.d) $$
$\hat\alpha$有效性的要求：$$\alpha_{it} \quad i.i.d.(关于t \quad i.i.d) $$
### 2.2. 可能影响一致性的因素和补救措施
#### 变量误差问题：影响$\hat{\beta}$
Shanken修正

使用投资组合$\hat\beta$值，以抵消个股$\hat\beta_i$估计的偏差：fm1973

使用公司特征取值直接作为$\hat\beta_i$(石川，需要补一个例子)
#### 残差项时序相关问题：影响$\hat\lambda$
Newey West标准误

## 3. Stata 实现
### 3.1. `xtfmb`
### 3.2. `asreg`
### 3.3. `fm` 
这三个命令实际上都只是**做了第二步截面回归**，都可以汇报Newey-West标准误。
比如asreg帮助文档：
>The first step involves estimation of N cross-sectional regressions and the second steps involves T time-series averages of the coefficients of the N-cross-sectional regressions

这里的第一步是截面回归，其实是我们通常解释的第二步。官网给的例子：
``` stata
asreg invest mvalue kstock, fmb first
```
忽略变量本身含义，第一阶段(First stage Fama-McBeth regression results)报告的是各年度截面回归得到的风险溢价估计$\hat\lambda_t$，Two-Step procedure报告的是第一阶段$\hat\lambda_t$时序上的均值。**这里的自变量应该是估计好的$\hat\beta$**.




## 4. 实操中可能存在的问题

### 4.1. 第一步回归的分组和滚动窗口:
**想主要解决这个问题： [Trailing ten-year Fama-MacBeth slope coefficients](https://stackoverflow.com/questions/40533595/trailing-ten-year-fama-macbeth-slope-coefficients)**

分组：`statsby _b _se, by(company): reg r_i rmkt` 可以用于第一步回归
参考：[Stata: 获取分组回归系数的三种方式](https://zhuanlan.zhihu.com/p/108214139)

滚动窗口：需要自己写，可能用到`rolling`, `statsby`, `asreg`
`bys company: asreg r_i r_mkt, wind(year 10)` ? 



### 4.2. 上面三种方法都不计算Shanken修正的标准误
[code for Fama-Macbeth with Shanken (1989) correction](https://www.statalist.org/forums/forum/general-stata-discussion/general/1354925-code-for-fama-macbeth-with-shanken-1989-correction)

目前没想到什么解决方法

## 5. References：(内容&格式待完善)

Asset Pricing. Ch.12 Regression-Based Tests of Linear Factor Models

Size and value in China.Jianan Liu, Robert F. Stambaugh, Yu Yuan Journal of Financial Economics 2019



