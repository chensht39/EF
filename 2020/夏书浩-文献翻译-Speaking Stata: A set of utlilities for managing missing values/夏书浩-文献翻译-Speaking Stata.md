> &#x1F449;  点击右上方的【编辑】按钮，可以查看 Markdown 原始文档。

&emsp;

> **夏书浩**：中山大学  
> **E-Mail:**<1012021558@qq.com>  

&emsp;

---

**Source：**[Speaking Stata: A set of utlilities for managing missing values](http://sage.cnpereading.com/paragraph/article/?doi=10.1177/1536867X1501500413)
# 标题：Speaking Stata: A set of utlilities for managing missing values
&emsp;
## 作者： Nicholas J. Cox
&emsp;
## 摘要：
识别缺失值的频率与模式以及对缺失数据的初步清理是统计数据管理的常见且基本的部分。
我提供了一种新的命令 `missings` ，作为我以前命令 `nmissing` 和 `dropmiss` 的替代与延伸。
&emsp;
## 关键词：
dm0085 ，缺失，缺失值，系统性缺失，扩展缺失值，数据管理，数值变量，字符串变量，`drop`，`list`，`table`，`tag` 
&emsp;
## 1. 前言

统计软件的一个重要特征是对缺失值的支持。大量真实的数据集，即使是高质量的数据集，
也可能因为好的或坏的原因而出现零星的数据缺失。人们不能回答所有的问题，仪器可能会坏掉或无法记录，观察者或实验者可能无法进行某些测量:失踪的原因很多;失踪的事实是普遍的。在数据集中保存这些缺失值的能力是必不可少的，即使这意味着忽略统计或图形分析中的缺失。

良好的支持意味着缺失值的特殊代码。一种可以作为替代的方法是采用一些特别的惯例
（遗漏年龄被记录为 -99 岁，或其他），采用这样的数据未免有些尴尬，也容易出错。

因此，对于缺失值解决方法的特殊代码长期以来都为统计软件的编写者所使用的。总的来说，首要原则就是
缺失值与非缺失值总是可辨别的。第二原则则是它可以很大程度上为缺失值分离出不同的原因。特别地，
在 Stata 中，我们使用系统性缺失变量 **.** ，以及数值型变量的扩展缺失值，从 **.a** 到 **.z** ， 
字符串变量的空白字符串""。

对于缺失值的谨小慎微通常被认为太过明显或者常见，不值得在文本中讨论。[Altman （1991）](https://www.researchgate.net/publication/24917811_Practical_Statistics_for_Medical_Research)和 [Long （2009）](https://www.stata-press.com/books/workflow-data-analysis-stata/)
则对此有其他的见解，他们对各种陷阱提出了明确的警告，并详细阐述了关于 Stata 的建议。

Stata 中的几个命令可以提供处理缺失值的帮助。这些包括：

- `codebook` ([D] **codebook**)，计算缺失值的数目，从而提供更多细节；

- `egen` ([D] **egen**)，可以计算缺失值的数目，也可以计算非缺失值的数目；

- `ipolate` ([D] **ipolate**)，用于插入序列中给定的缺失值；

- `misstable` ([R] **misstable**)，（在 Stata 11 中添加），用于报告缺失值；

- `mvdecode` ([D] **mvencode**) 和 `recode` ([D] **recode**)，在这里提到的主要是它们
将非缺失值重新编码为缺失值，特别是处理导入数据时的作用（比如 -99 岁可能意味着缺失）；

- `tabulate` ([R] **tabulate**) 和 `tabulate twoway` ([R] **tabulate twoway**)，以及
它们的姊妹代码 `tab1` 和 `tab2` ，它们有着有效的遗漏选项可以将遗漏变量也计算在内。
 
在处理缺失值时，最重要的帮助是在 Stata 11 中添加的用于多重赋值的 `mi` ([MI] **intro**)，
它在之后的版本中也得到了增强。

在用户书写的可能有助于处理缺失值的命令中，我任意挑选做出了 `findname` [Cox 2010a,b,2012,2015] 
其功能包括找出全部、某些或无缺失值的变量，以及 `mipolate` ，命令 `ipolate` 的扩展，在编写中
可从统计软件组件中存档下载。

在此，我关注到了命令 `missings`，一组用于管理缺失值的实用程序。作为我以前命令 `nmissing` [[Cox
1999,2001a,2003,2005]](https://journals.sagepub.com/) 和 `dropmiss` [[Cox 2001b,2008]](https://journals.sagepub.com/)的替代与延伸。 `missings` 包括了一些新的
功能，但排除了一些现在看来价值有限的细节。（任何想了解这些细节的人都会发现之前的命令依然是可以
访问和下载的。尽管没有什么必要或暗含的联系， `missings` 的设计确实是受到了命令 `duplicates` 的影响，
它提供了一系列微型但相当实用的程序来处理重复观察值。

我将大概阐述 `missings`这一命令的功能并以一个正式的语句结束。

&emsp;

## 2. 命令目标

`missings` 组合了各种基本实用程序来管理可能缺失值的把变量。语句开始于选择六个 `missings` *子命令*。
在默认情况下，"missing" 正如前一节所定义的，是全部丢失的意思。之后，我们将学习如何改变这一默认设定。

如果 *varlist* (variable list) 没有进一步说明，那么在默认情况下它就被解释为所有的变量。

`missing report` 可以发布一份关于在 *varlist* 中缺失值数量的报告。在默认情况下，缺失数由变量给出；可供选择地，
计数则由观察结果给出。

`missing list` 列出了 *varlist* 中含有缺失值的变量。

`missing table` 根据 *varlist* 中缺失值的数量将观察结果列成表。

`missing tag` 生成一个变量，其中包含每个观察不变列表中缺失值的数量。

`missings dropvars` 删除在所有值上确实的所有变量。

`missings dropobs` 删除了所有在 *varlist* 上含有缺失值的变量。

让我们考虑一下在整个观察或变量中可能出现的缺失。创建完全空的的观察值（行）和变量（列）示许多电子表格程序用户的习惯。
主要原因就是讲数据块分割开以便于理解和处理。即使电子表格内容导出为完全不同的文件格式，这些空白行和列也可以不加修饰地复制。
然而，任何一种空白行在 Stata 数据集中都是不可用的。最差的是，这种变量或观察值会消耗以前的记忆。所以子命令 `dropobs` 和 
`dropvars` 可以帮助用户清理这些变量。

注意，对于删除含有一些缺失值但也有非缺失值的观察值和变量时， `missings` 是并不支持的。研究人员经常试图将数据集减少到一个最小值，
在变量和观察结果上完全不丢失。这种策略可能会严重减少（变量与观察结果）。作为一名程序作者，我希望用户对它们可能对数据集造成的
任何损害承担全部责任。那些执意破坏数据的用户可以在 `missings` 的其他子命令中寻找到一些中间步骤。现代统计思维鼓励考虑多重归因，这是一个
更好的发展方向。任何一种统计思维都会鼓励你利用你所拥有的所有信息，尽管它们存在缺陷。

用户对于缺失值可能有超越 Stata 定义的、他们自己观点。举例来说，那些只有一个空格或更多空格的字符串变量是没有意义的。然而，因为它们
是非空的，它们并没有被定义为缺失。假如你希望将他们视为缺失值，最简单的方式就是使用命令 `trim()` 来处理那些含有一个或多个空格产生
的空字符串。正如在第一部分提到的命令 `findname`，可以通过如下代码找到所有这样可调的变量：

-    `findname, any(trim(@)==""&@!="")`

更简单的一种形式为：

-    `findname, any(trim(@)=="")`

他们可以找到那些只有空格的空白字符串和变量。`findname` 同样有一个 `all()` 选项来寻找那些满足其他准则的变量。一旦这种变量被定义，
你就应该使用 `trim()` 来移除这些空格。`findname` 自身并不做对变量的整理；它仅仅报告结果。`missings` 没有对超过 Stata 对缺失值的
定义进行延展，所以一些变量在使用前需要进行清理。

另一种相似的例子是用 "**.**" 来代替字符串的缺失值。尽管 Stata 确实对数值型变量的系统性缺失赋予了 "**.**" 特殊的含义，不过仅由
点组成的字符串并没有特殊的含义。在使用 `missings` 之前，这个货任何其他关于缺失值的个人约定都需要特殊的操作。

我重申，`mvdecode`([D] **mvencode**) 是定制用于将 Stata 自己的缺失值替换为指示缺失的数字代码。

&emsp;

## 3. `missings` 的实例

在之前的概述之后，我们来看看关于命令 `missings` 的一系列实例。我们可以读取一个相当复杂的数据集作为一个
沙盒，在其中展示并获得一个关于缺失值的简要报告。我们可以指定一个预知未来看看哪些变量看起来特别差。

```stata

. webuse nlswork
(National Longitudinal Survey.  Young Women 14-26 years of age in 1968)

. missings report
Checking missings in all variables:
15082 observations with missing values

age           24
msp           16
nev_mar       16
grade          2
not_smsa       8
c_city         8
south          8
ind_code     341
occ_code     121
union       9296
wks_ue      5704
tenure       433
hours         67
wks_work     703

. missings report, minimum(1000)

Checking missings in all variables:
15082 observations with missing values

union     9296
wks_ue    5704

```

尽管在此没有显示，`missings report`也支持展示百分比记数。

我们可以通过请求列表来寻找缺失值的观察结果。同样，我们可以指定一个阈值：

```stata

. missings list, minimum(5)

Checking missings in all variables:
15082 observations with missing values

       +-------------------------------------------------------------------------------+
 6924. | age | msp | nev_mar | not_smsa | c_city | south | ind_code | occ_code | union |
       |  26 |   . |       . |        0 |      1 |     0 |       11 |        8 |     . |
       |-------------------------------------------------------------------------------|
       |      wks_ue      |        tenure      |      hours      |      wks_work       |
       |           .      |      .0833333      |         40      |             .       |
       +-------------------------------------------------------------------------------+

       +-------------------------------------------------------------------------------+
19002. | age | msp | nev_mar | not_smsa | c_city | south | ind_code | occ_code | union |
       |  31 |   . |       . |        0 |      1 |     1 |        7 |        8 |     . |
       |-------------------------------------------------------------------------------|
       |      wks_ue      |        tenure      |      hours      |      wks_work       |
       |           .      |      .3333333      |          .      |             .       |
       +-------------------------------------------------------------------------------+

       +-------------------------------------------------------------------------------+
21220. | age | msp | nev_mar | not_smsa | c_city | south | ind_code | occ_code | union |
       |  26 |   1 |       0 |        . |      . |     . |        7 |        3 |     . |
       |-------------------------------------------------------------------------------|
       |      wks_ue      |        tenure      |      hours      |      wks_work       |
       |           .      |      .0833333      |         40      |             .       |
       +-------------------------------------------------------------------------------+

       +-------------------------------------------------------------------------------+
22493. | age | msp | nev_mar | not_smsa | c_city | south | ind_code | occ_code | union |
       |  32 |   1 |       0 |        1 |      0 |     0 |        . |        . |     . |
       |-------------------------------------------------------------------------------|
       |      wks_ue      |        tenure      |      hours      |      wks_work       |
       |           .      |           .75      |          .      |             .       |
       +-------------------------------------------------------------------------------+

       +-------------------------------------------------------------------------------+
22628. | age | msp | nev_mar | not_smsa | c_city | south | ind_code | occ_code | union |
       |  39 |   0 |       0 |        0 |      1 |     0 |        . |        3 |     . |
       |-------------------------------------------------------------------------------|
       |      wks_ue      |        tenure      |      hours      |      wks_work       |
       |           .      |           1.5      |          .      |             .       |
       +-------------------------------------------------------------------------------+

```

作为一个简单的例子，我们只是将门槛设置得高一些。在这个例子中，只有5个观察值有5个或更多的缺失值。当然，你需要
的阈值将取决于你的数据集和目标。同样。产生的清淡可能需要更长时间的仔细审查。

另一个基本命令，`missings table`，则报告所有指定变量的缺失记数：

```stata

. missings table

Checking missings in all variables:
15082 observations with missing values

       # of |
    missing |
     values |      Freq.     Percent        Cum.
------------+-----------------------------------
          0 |     13,452       47.14       47.14
          1 |     13,790       48.33       95.47
          2 |        964        3.38       98.85
          3 |        291        1.02       99.87
          4 |         32        0.11       99.98
          5 |          2        0.01       99.99
          6 |          3        0.01      100.00
------------+-----------------------------------
      Total |     28,534      100.0

```

在适当的地方，您可以指定一个 `by`，这一前缀使得可以按其他变量细分：

```stata

. bysort race: missings table

-----------------------------------------------------------------------------------------
-> race = 1

Checking missings in all variables:
10576 observations with missing values

       # of |
    missing |
     values |      Freq.     Percent        Cum.
------------+-----------------------------------
          0 |      9,604       47.59       47.59
          1 |      9,672       47.93       95.52
          2 |        677        3.35       98.88
          3 |        199        0.99       99.86
          4 |         25        0.12       99.99
          5 |          1        0.00       99.99
          6 |          2        0.01      100.00
------------+-----------------------------------
      Total |     20,180      100.00

-----------------------------------------------------------------------------------------
-> race = 2

Checking missings in all variables:
4342 observations with missing values

       # of |
    missing |
     values |      Freq.     Percent        Cum.
------------+-----------------------------------
          0 |      3,709       46.07       46.07
          1 |      3,966       49.26       95.33
          2 |        278        3.45       98.78
          3 |         89        1.11       99.89
          4 |          7        0.09       99.98
          5 |          1        0.01       99.99
          6 |          1        0.01      100.00
------------+-----------------------------------
      Total |      8,051      100.00

-----------------------------------------------------------------------------------------
-> race = 3

Checking missings in all variables:
164 observations with missing values

       # of |
    missing |
     values |      Freq.     Percent        Cum.
------------+-----------------------------------
          0 |        139       45.87       45.87
          1 |        152       50.17       96.04
          2 |          9        2.97       99.01
          3 |          3        0.99      100.00
------------+-----------------------------------
      Total |        303      100.00

```

这有时对变量的缺失进行记数是有帮助的。正如在第一节中提到的。尽管已经有了命令 `egen`，但是稍微
重复一下也无妨。

```stata

. missings tag, generate(nmissing)

Checking missings in all variables:
15082 observations with missing values

```

由 `misssings tag` 生成的变量，正如由 `egen` 函数 `rowmiss()` 生成的变量一样，只有当观测值中
没有缺失值时，才可以为零。在 Stata 中，当为真或假的决策提供参数时，[零被视为假，正被视为真。](http://www.stata.com/support/faqs/data-management/ture-and-false/)
因此，像 `...if tag` 这样的命令将筛选出含有缺失值的观测结果；以及它的否定 `...if !tag` 将挑选出没有缺失值的观测结果。如果你喜欢其他形式，也可以
将其写为 `...if tag>0` 或 `...if tag ==0`。注意，你可以将一变量写作（0,1）二值变量：

`. generate anymissing = nmissing > 0`

这个数据集并没有完全丢失的变量或观测数据，但是可以用 `missings` 来删除这些变量或观测数据。我们将通过添加一些无意义的变量来扩展我们
所展示的沙盒。

```stata

. generate newt = ""
(28,534 missing values generated)

. generate frog = .
(28,534 missing values generated)

. generate toad = .a
(28,534 missing values generated)

```

这些新的变量是不同类型缺失值的简单例子，分别包含了：仅有空字符串，仅有系统性缺失，以及仅有其中一个扩展缺失值。

如果我们认为那些延伸的缺失值是有意义的且应当囊括在内，而那些空字符串和系统性缺失值则是无意义的，应当被删除（只要没有非缺失值在同一
变量中被观测到）。为了完成这一任务，我们需要 `sysmiss` 这一选项，它将忽略空字符串。（等价地，我们延伸了 Stata 对于“系统性缺失”的常规定义，
以包含空字符串。另一种选择是引入一些新的术语，这可能显得武断或神秘，例如“真的缺失”或“完全缺失”。

```stata

. missings dropvars newt frog toad, force sysmiss

Checking missings in newt frog toad:
28534 observations with system missing values

note: newt frog dropped

```

在我们的例子中， **newt** 和 **frog** 是完全无意义并可以删除的。然而，注意到额外的选项 `force`。当内存中的数据集从上次保存的版本
更改时，你需要使用选项 `force` 删除任何变量或观测结果，即使他们是完全缺失的。这似乎过于谨慎，但谨慎要求对数据集的任何破坏性更改都要
得到确认。

`missings` 不会删除掉 **toad**， 一个仅仅包括 **.a** 的变量，这是因为它受到选项 `sysmiss` 的保护。如果我们在尝试一次：

```stata


. missings dropvars toad, force sysmiss

Checking missings in toad:
0 observations with system missing values

note: no variables qualify

```

如果我们移除这一保护，**toad** 将会被从数据集中移除：

```stata

. missings dropvars toad, force

Checking missings in toad:
28534 observations with missing values

note: toad dropped

```

让我们继续我们的沙盒，并模拟所有缺失的观察结果。如果我们扩大数据集，将观测数量设置为 30000，那么就会产生 1466 个额外的观测，
这些观测在每个变量上缺失。

```stata

. set obs 30000
number of observations (_N) was 30,000, now 30,000

. missings dropobs, force

Checking missings in idcode year birth_yr age race msp nev_mar grade collgrad not_smsa
    c_city south ind_code occ_code union wks_ue ttl_exp tenure hours wks_work ln_wage
    nmissing:
16548 observations with missing values

(1,466 observations deleted)

```

和以前一样，这里使用了各种安全功能。最明显的是，如果内存中的数据集没有保存，那么你必须认识到你正在强行删除。此外，扩展缺失值 **.a** 到 **.z** 
应当是有意义的，所以你可以用 `sysmiss` 来说明只有系统性缺失才可能被删除。在这种情况下，`expand` 不会创建扩展的缺失值，因此也不会
出现问题。

至少以我的经验来看，很少有这么多的观察结果被完全忽略。但这一原则适用于大清理，也适用于小清理————事实上也适用于丢失的观测数据，不管它们是
故意在某一点还是仅仅是偶然插入的。

&emsp;

## 4. 命令语法

### 4.1 语法图

`missings report` [*varlist*] [*if*] [*in*] [,*common_options* **observations**
minimum(#) **percent format**(*format*) *list_options*]

`missings list` [*varlist*] [*if*] [*in*] [,*common_options* minimum(#) *list_options*]

`missings table` [*varlist*] [*if*] [*in*] [,*common_options* minimum(#) *tabulate_options*]

`missings tag` [*varlist*] [*if*] [*in*] , generate(*newvar*) [*common_options*]

`missings dropvars` [*varlist*] [,*common_options* **force**]

`missings dropobs` [*varlist*] [*if*] [*in*] [,*common_options* **force**]
 
*common_options* 包括数字型，字符串，和系统性缺失值。

**by**: 可以用于 `missings report`, `missings list`, 和 `missings table`。

&emsp

### 4.2 描述

`missings` 是一组使用的程序命令，用于管理可能丢失的变量。在默认情况下，对于数值型变量，"missing"意味着
数值型变量的缺失（即系统缺失值，或扩展缺失值之一，从 **.a** 到 **.z** ），而对于字符串变量，
则为空或""。

如果 *varlist* 并未指定，那么在默认情况下它代指所有变量。

&emsp;

### 4.3 选项

`numeric` （所有的子命令）指只包含数值型变量。如果显式命名了字符串变量，这些变量将被忽略。

`string` （所有的子命令）指只包含字符串变量。如果显式命名了数值型变量，这些变量将被忽略。

`sysmiss` （所有的子命令）指仅包括系统性缺失 **.** 。此选项对字符串变量不起作用，对于字符串变量，
不论如何缺失值都会被视为空字符串""。

`observations (missings report)` 指通过观察计数缺失的值，而不是默认的通过变量计数。

`minium(#) (missings report,missings list,and missings table)` 指定可以清楚展示的最小缺失值。
在 `missings table` 中，默认值是 **minimum(0)**；否则为 **minimum(1)**

`percent (missings report)` 报告缺失的百分比和计数。百分比是根据观察数据或指定变量的数量计算的。

`format`(*format*) `(missings report)` 指定百分比的显示格式。 默认是 `format(%5.2f)`。除非 `percent`
也被指定，否则该命令是无效的。

*list_options* (`missings report` and `missings list`) 是当 `list` 被用于展示结果时，在[D] `list` 中列出的选项。

*tabulate_options* (`missings table`) 是当 `tabulate` 被用于展示结果时，在[R] `tabulate` 中列出的选项。

`generate` (*newvar*) (`missings tag`) 指定了一个新变量的名称。`generate()` 是要求的。

`force` (`missings dropvars` and `missings dropobs`) 指正在更改内存中的数据集，并且当数据被删除
且内存中的数据集未按此方式保存时，这是必需的选项。

&emsp;

## 5. 结论

当你查找缺失值时，几个内置命令可以提供第一步。命令 `missings` 的目的是在第二步有所帮助，以便尽早
做出该做什么的决定————从放弃没有信息的变量或观察到考虑多重归因。最低限度来说，了解缺失可以为数据管理
和数据分析的策略提供信息。不了解缺失信息会产生很多困惑和误导的结果，所以好处是明晰的。

&emsp;

## 6. 鸣谢

Jeroen Weesie, Eric Uslaner, 和 Estie Sid Hudes 对于早期命令 `nmissing` 和 `dropmiss` 的贡献。

&emsp;

## 7. 参考文献

[Altman,D.G. 1991. *Practical Statistics for Medical Research*. London: Chapman & Hall](https://www.researchgate.net/publication/24917811_Practical_Statistics_for_Medical_Research)

[Cox, N. J. 1999. dm67: Numbers of missing and present values. *Stata Technical Bulletin*
49: 7–8. Reprinted in *Stata Technical Bulletin* Reprints, vol. 9, pp. 26–27. College
Station,TX: Stata Press.](https://ideas.repec.org/a/tsj/stbull/y2000v9i49dm67.html)

[————. 2001a. dm67.1: Enhancements to numbers of missing and present values. *Stata
Technical Bulletin* 60: 2–3. Reprinted in *Stata Technical Bulletin* Reprints, vol. 10,
pp. 7–9. College Station,TX: Stata Press.](https://ideas.repec.org/a/tsj/stbull/y2001v10i60dm67.1.html)

[————. 2001b. dm89: Dropping variables or observations with missing values. *Stata
Technical Bulletin* 60: 7–8. Reprinted in *Stata Technical Bulletin* Reprints, vol. 10,
pp. 44–46. College Station,TX: Stata Press.](https://ideas.repec.org/a/tsj/stbull/y2001v10i60dm89.html)

[————. 2003. Software Updates: dm67_2: Numbers of missing and present values. *Stata
Journal* 3: 449.](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0400300415)

[————. 2005. Software Updates: dm67_3: Numbers of missing and present values. *Stata
Journal* 5: 607.](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0500500413)

[————. 2008. Software Updates: dm89_1: Dropping variables or observations with
missing values. *Stata Journal* 8: 594.](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800416)

[————. 2010a. Software Updates: dm0048_1: Finding variables. *Stata Journal* 10:
691–692.](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101000412)

[————. 2010b. Speaking Stata: Finding variables. *Stata Journal* 10: 281–296.](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1001000208)

[————. 2012. Software Updates: dm0048_2: Finding variables. *Stata Journal* 12: 167.](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1201200113)

[————. 2015. Software Updates: dm00483: Finding variables. *Stata Journal* 15: 605–
606.](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1501500220)

[Long, J. S. 2009. *The Workflow of Data Analysis Using Stata*. College Station, TX:
Stata Press.](https://www.stata-press.com/books/workflow-data-analysis-stata/)

&emsp;

### 关于作者

Nicholas Cox 是Durham University 极具统计学头脑的地理学家。他为 Stata 用户社区提供演讲、帖子、常见
问题和程序。他还在 Stata 官方中合作撰写了15条命令。他是 *Stata Journal* 数篇文章的作者，也是该
期刊的编辑。他从 2004 年到 2013 年关于图形的" Speaking Stata" 文章已经被收录为 *Speaking Stata Graphics*。