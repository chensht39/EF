*增加选项 format()，以便用户自定义结果的显示格式
*------------------------begin---myfact3.ado----------------------*
capture program drop myfact3
program define myfact3
version 15

syntax anything(name=k) [, Format(string)]

  local a = 1
  forvalues i = 1/`k'{
    local a = `a'*`i'
  }
  
  dis "`k'! = " `format' `a' 

end 
*------------------------end-----myfact3.ado----------------------*
