*直接调用 Mata 自带的函数factorial()计算阶乘
*------------------------begin---myfact8.ado----------------------*
capture program drop myfact8
program define myfact8
version 15
args k    // 定义暂元 k，用于接收用户输入的数字
mata:factorial(`k')
end

myfact8 5
*------------------------end-----myfact8.ado----------------------*
