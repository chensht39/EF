> &#x1F449;  点击右上方的【编辑】按钮，可以查看 Markdown 原始文档。

&emsp;

>   


> **作者：** 郭瀚中（中山大学岭南学院）&emsp;卢文杰（中山大学岭南学院）       
> **E-mail:** <guohzh6@mail2.sysu.edu.cn>  &emsp;  <luwj25@mail2.sysu.edu.cn>   
> 
> **指导老师：** 连玉君 （中山大学岭南学院，[arlionn@163.com](mailto:arlionn@163.com)）
> 周先波 （中山大学岭南学院，[zhouxb@mail.sysu.edu.cn](mailto:zhouxb@mail.sysu.edu.cn)）
&emsp;

---

**目录**

[TOC]

---

&emsp;

## 1. 背景
&emsp;前言：在没有交叉项线性模型中，回归得到的系数即为解释变量对被解释变量的边际效应，但是对于估计 `probit` , `logit` 等非线性模型的边际效应，我们应该采用什么样的估计和检验方法呢？更进一步，如果解释变量是内生变量，我们应该如何处理？

&emsp;&emsp;假设当我们想研究子女是否能考上一本大学与家庭收入水平、父母受教育水平的关系。在这一问题中，子女是否能考上一本大学是一个二元被解释变量，自变量则包括家庭收入水平、父母受教育水平，我们还另外引入其他控制变量如性别、民族等。家庭收入水平和父母受教育水平之间有相关关系，另外，家庭收入水平还可能受到其他不可观测因素的影响，可能出现内生性。那么我们应如何研究该问题呢？

&emsp;&emsp;在实证分析中，对于二元被解释变量的分析常采用`probit`或`logit`模型。虽然回归结果表格中的变量的系数估计值反映了该变量对被解释变量影响作用的大小。但是，当回归模型中包含类别变量、交乘项或者回归模型为非线性（诸如Logit,Probit等非线性模型）时，对系数估计值的解释就非常具有挑战性。这时，就需要计算变量的边际效应或者计算预测边际值，以探求自变量变化对因变量变化的影响作用或分析比较不同情况时的因变量预测边际值的大小。目前有许多文章研究`probit`,`logit`等非线性模型中边际效应、交互效应的估计问题，不过以往研究的情况均为解释变量是外生的。

&emsp;&emsp;在较多情况下，解释变量和被解释变量会同时受到一些不可观测因素的影响，或者被解释变量反向影响解释变量，此时解释变量就很难满足外生性要求。那么在有解释变量是内生的情形时，怎样进行Probit,Logit等非线性模型中边际效应、交互效应或平方效应的估计和检验呢？进一步，如果模型中包含了内生变量的交乘项，在实证操作中又该如何处理？如在考察调节效应时，核心解释变量对被解释变量的边际影响将受到第三个变量取值的影响，此时往往需要引入交互项。对此，在本推文中，我们将着重对内生变量的交互项如何处理这一问题进行阐述。[Zhou(2021)](https://www.sciencedirect.com/science/article/pii/S0165176520304559?dgcid=author.pdf/)在传统的Probit模型里边考虑一个内生变量以及这个内生变量和其他变量的交乘项，基于内生非线性模型给出了变量的平均边际效应、含内生变量的交乘项平均交互效应、外生平均平方效应的方法。基于该文章，我们编写了一个新命令：`eivprobit`，实现Probit内生非线性模型估计上述平均边际效应、上述交互效应、上述平方效应的估计和检验。

&emsp;

## 2. 模型介绍
>首先本文对[Zhou (2021)](https://www.sciencedirect.com/science/article/pii/S0165176520304559?dgcid=author.pdf/) 基于内生非线性模型给出了变量的平均边际效应、含内生变量的交乘项平均交互效应、外生平均平方效应的方法进行介绍。

$$
y = 1\left\{\alpha_{1}x_{1}+\alpha_{2}x_{2}+\alpha_{3}x_{1}x_{2}+x^{\prime}\beta+\varepsilon>0\right\}
\quad (1)
$$

>式（1）即为 `probit` 模型，$x_{1}$ 是内生变量, $x_{2}$ 是外生变量, $x_{1}$ 和 $x_{2}$ 形成交互项,x 是其它外生变量形成的向量。假设 $x_{1}$ 是连续的。$z$ 是 $x_{1}$ 的多个代理变量形成的向量。即可得到 $x_{1}$ 关于 $x_{2}$、$x$ 和 $z$ 的回归方程：

$$
x _ { 1 } = \gamma _ { 2 } x _ { 2 } + x ^ { \prime } \gamma + z ^ { \prime } \delta + v _ { 1 }
\quad (2)
$$

$$
令 \varepsilon = \theta _ { 1 } v _ { 1 } + e ,其中 \quad e \sim N \left( 0 , \sigma _ { e } ^ { 2 } \right)
$$
>$\varepsilon$和 $v_1$ 都分别与 $x_2$, $x$, $z$ 相互独立，且服从正态分布。$e$ 和 $x_1$, $x_2$, $x$, $v_1$ 相互独立,将其代入 `probit` 模型并变形即可得到

$$
y = 1\left\{\frac{\alpha_{1}}{\sigma_{e}}x_{1}+\frac{\alpha_{2}}{\sigma_{e}}x_{2}+\frac{\alpha_{3}}{\sigma_{e}}x_{1}x_{2}+x^{\prime}\frac{\beta}{\sigma_{e}}+\frac{\theta_{1}}{\sigma_{e}}v_{1}+\frac{e}{\sigma_{e}}>0\right\}
\quad (3)
$$

>通过简单转化变形即可得到
$$
\begin{array} { c } P \left( y = 1 \mid x _ { 1 } , x _ { 2 } , x , v _ { 1 } \right) = \Phi \left( \frac { \alpha _ { 1 } x _ { 1 } + \alpha _ { 2 } x _ { 2 } + \alpha _ { 3 } x _ { 1 } x _ { 2 } + x ^ { \prime } \beta + \theta _ { 1 } v _ { 1 } } { \sigma _ { e } } \right) \\ P \left( y = 1 \mid x _ { 1 } , x _ { 2 } , x \right) = E _ { v _ { 1 } } \left[ \Phi \left( \frac { \alpha _ { 1 } } { \sigma _ { e } } x _ { 1 } + \frac { \alpha _ { 2 } } { \sigma _ { e } } x _ { 2 } + \frac { \alpha _ { 3 } } { \sigma _ { e } } x _ { 1 } x _ { 2 } + x ^ { \prime } \frac { \beta } { \sigma _ { e } } + \frac { \theta _ { 1 } } { \sigma _ { e } } v _ { 1 } \right) \right] \end{array}
\quad (4)
$$

>实际上该模型分为两步完成
 (i)$x_1$ 对 $x_2$, $x$, $z$ 回归得到 $\hat{v_1}$; 
 (ii)对 $x_1$, $x_2$, $x_{1}x_{2}$, $x$, $\hat{v_1}$ 进行 `probit` 回归得到回归系数

$$
\hat { \lambda } \equiv \left( a _ { 1 } , a _ { 2 } , a _ { 3 } , b ^ { \prime } , \hat { \theta } _ { 1 } \right) ^ { \prime }
$$

>通过证明可知（3）式的渐进估计即为
$$
\begin{array} { l } P \left( x _ { 1 } , x _ { 2 } , x ; \hat { \lambda } \right) \equiv \frac { 1 } { n } \sum _ { i = 1 } ^ { n } \Phi \left( \tau _ { i } \right) ,其中 \tau _ { i } \equiv a _ { 1 } x _ { 1 } + a _ { 2 } x _ { 2 } + a _ { 3 } x _ { 1 } x _ { 2 } + x ^ { \prime } b + \hat { \theta } _ { 1 } \hat { v } _ { 1 i } \end{array}
$$
>之后便可求得 $x_1$ 和 $x_2$ 在 Probit 模型中的平均边际效应。
$$
\begin{array} { c } \operatorname { MPE } _ { 1 } \left( x _ { 1 } , x _ { 2 } , x ; \hat { \lambda } \right) \equiv \frac { \partial P \left( x _ { 1 } , x _ { 2 } , x ; \hat { \lambda } \right) } { \partial x _ { 1 } } = \left( a _ { 1 } + a _ { 3 } x _ { 2 } \right) \frac { 1 } { n } \sum _ { i = 1 } ^ { n } \phi \left( \tau _ { i } \right) \\ \operatorname { MPE } _ { 2 } \left( x _ { 1 } , x _ { 2 } , x ; \hat { \lambda } \right) \equiv \frac { \partial P \left( x _ { 1 } , x _ { 2 } , x ; \hat { \lambda } \right) } { \partial x _ { 2 } } = \left( a _ { 2 } + a _ { 3 } x _ { 1 } \right) \frac { 1 } { n } \sum _ { i = 1 } ^ { n } \phi \left( \tau _ { i } \right) \\ \operatorname { inteff } \left( x _ { 1 } , x _ { 2 } , x ; \hat { \lambda } \right) \equiv \frac { \partial ^ { 2 } P \left( x _ { 1 } , x _ { 2 } , x ; \hat { \lambda } \right) } { \partial x _ { 1 } \partial x _ { 2 } } = \frac { 1 } { n } \sum _ { i = 1 } ^ { n } \left[ a _ { 3 } - \left( a _ { 1 } + a _ { 3 } x _ { 2 } \right) \left( a _ { 2 } + a _ { 3 } x _ { 1 } \right) \tau _ { i } \right] \phi \left( \tau _ { i } \right) \end{array}
\quad (5)
$$
>`eivprobit` 命令即是基于该模型实现在 $x_1$ 内生情况下计算 $x_1$ 和 $x_2$ 在 Probit 模型中的平均边际效应以及 $x_1$ 和 $x_2$ 交乘项平均交互效应。另外，通过 `eviprobit` 命令的 option 也可以计算出控制变量向量 $x$ 中某个变量的平均边际效应以及平均平方效应。例，当 option 选项中输入 `maeffect(x3)` ,即可计算出 $x_3$ 的平均边际效应。当 option 选项中输入 `quaeffect(x3)`，即可计算出 $x_3$ 的平均边际效应和平均平方效应。


## 3. `eivprobit` 命令介绍

### 3.1. 在 Stata 中安装 github 命令
>在 Stata 的 command 框口中输入如下命令自动下载外部命令 `github`. 该命令用于搜索、安装 Github 上发布的 Stata 外部命令 (若已经安装 `github` 命令，可以忽略此步)：
```stata
. net install github, from("https://haghish.github.io/github/")
```

### 3.2 `eivprobit` 命令下载


```Stata
. ssc install eivprobit
```
>完成安装后，可以进一步输入如下命令获取帮助文件：
```Stata
. help eivprobit
```


### 3.3 如何调用执行 eivprobit 命令呢？
>`eivprobit` 的语法结构如下：
```stata
. eivprobit depvar [varlist1] (variable1 = varlist_iv)   /// 
[if] [in], interact(variable 2) [options]
```

其中:
- `depvar` 是被解释变量,为0-1变量
- `variable 1` 是内生变量(目前只能写入一个)
- `variable 2` 是与内生变量进行交互乘积的外生变量
- `Varlist 1` 是控制变量,均为外生变量
- `varlist_iv` 是除`variable 2`以外的工具变量.
- `if` 是设定样本范围的条件语句
- `in` 用于设定观察值范围
- 不能在`Varlist 1`中写入`variable 2`
  

### 3.4 选项
| **具体命令**   | **解释** |
|-----------|-------------|
| `maeffect(varname)`      | 只能输入`Varlist 1`中的一个变量,具体模型参见(1)  |
| `quaeffect(varname)`      | 只能输入`Varlist 1`中的一个变量,具体模型参见(2)  |
| `seed(#)` |  设置随机数种子为 # |
| `bootstrap(#)` | 设置bootstrap的次数为 #, 默认为50次 |


详情请参考 `help eivprobit` 

----------

>--例：当输入选项选择`maeffect(x3)`或者不输入选项,模型如下：
$$
P ( y = 1 \mid x ) = \Phi \left( a _ { 1 } x _ { 1 } + a _ { 2 } x _ { 2 } + a _ { 3 } x _ { 1 } x _ { 2 } + a _ { 4 } x _ { 3 } + x ^ { \prime } \beta + \theta _ { 1 } v _ { 1 } \right)
\quad (6)
$$
>--当输入选项选择`quaeffect(x3)`,模型如下：
$$
p ( y = 1 \mid x ) = \Phi \left( a _ { 1 } x _ { 1 } + a _ { 2 } x _ { 2 } + a _ { 3 } x _ { 1 } x _ { 2 } + a _ { 4 } x _ { 3 } + a _ { 5 } x _ { 3 } ^ { 2 } + x ^ { \prime } \beta + \theta _ { 1 } v _ { 1 } \right)
\quad (7)
$$

&emsp;
## 4. 核心计算的子程序的介绍
>`eivprobit` 命令的计算过程中多次用到的计算程序是：已知 $x$ 和 $v$ 两个变量，生成 $V_{1}$ 和 $V_{2}$ 这两个变量。
$$
\mathcal { X } = \left( \begin{array} { l } x _ { 1 } \\ x _ { 2 } \\ \vdots \\ x _ { n } \end{array} \right) , \quad \mathcal { V } = \left( \begin{array} { l } v _ { 1 } \\ v _ { 2 } \\ \vdots \\ v _ { n } \end{array} \right)
$$
$$
V _ { 1 } = \left[ \begin{array} { l } \frac { 1 } { n } \sum _ { i = 1 } ^ { n } \Phi \left( x _ { 1 } + v _ { i } \right) \\ \frac { 1 } { n } \sum _ { i = 1 } ^ { n } \Phi \left( x _ { 2 } + v _ { i } \right) \\ \vdots \\ \frac { 1 } { n } \sum _ { i = 1 } ^ { n } \Phi \left( x _ { n } + v _ { i } \right) \end{array} \right] , \quad V _ { 2 } = \left[ \begin{array} { l } \frac { 1 } { n } \sum _ { i = 1 } ^ { n } \left( x _ { 1 } + v _ { i } \right) \Phi \left( x _ { 1 } + v _ { i } \right) \\ \frac { 1 } { n } \sum _ { i = 1 } ^ { n } \left( x _ { 2 } + v _ { i } \right) \Phi \left( x _ { 2 } + v _ { i } \right) \\ \vdots \\ \\ \frac { 1 } { n } \sum _ { i = 1 } ^ { n } \left( x _ { n } + v _ { i } \right) \Phi \left( x _ { n } + v _ { i } \right) \end{array} \right]
$$
>比较直观的方法便是通过循环进行遍历，便可解决实现该计算程序。但该方法在数据量比较大时需要比较长的运算时间。其次便是矩阵运算的思路，但由于个人电脑的运行内存有限，其在单次运算中可储存的矩阵大小有限制，当数据量比较大时， Stata 便会报错。我们设计了子程序 `SuperX` ，其通过在 Stata 里的 Mata 中进行矩阵分块运算实现该计算步骤，在极大地缩短运算时间的同时解决了运行内存无法支撑大数据量运算的问题。矩阵分块运算的主要思路便是，假设要计算 $V_{1}$ 变量中的第 $m+1$到$m+k$ 个数据。先生成矩阵 $V$，以及从列变量x中提取出列向量 $X$。
$$
V = \left( \begin{array} { c } v _ { 1 } , v _ { 2 } , \ldots , v _ { n } \\ v _ { 1 } , v _ { 2 } , \ldots , v _ { n } \\ \ldots \ldots \ldots \ldots \ldots \\ \ldots \ldots \ldots \ldots \ldots \\ v _ { 1 } , v _ { 2 } , \ldots , v _ { n } \end{array} \right) _ { k \times n } , \quad X = \left( \begin{array} { c } x _ { m + 1 } \\ x _ { m + 2 } \\ \ldots \\ x _ { m + k } \end{array} \right)
$$
>然后将列向量 $X$ 中的元素逐列加到V的每一列，得到 $XV$ 矩阵。
$$
X V = \left( \begin{array} { l } x _ { m + 1 } + v _ { 1 } , x _ { m + 1 } + v _ { 2 } , \ldots , x _ { m + 1 } + v _ { n } \\ x _ { m + 2 } + v _ { 1 } , x _ { m + 2 } + v _ { 2 } , \ldots , x _ { m + 2 } + v _ { n } \\ \ldots \ldots \ldots \ldots \ldots \\ \ldots \ldots \ldots \ldots \ldots \\ x _ { m + k } + v _ { 1 } , x _ { m + k } + v _ { 2 } , \ldots , x _ { m + k } + v _ { n } \end{array} \right) _ { k \times n }
$$
>对 $XV$ 矩阵每个元素求正态累计分布，得到矩阵 $Normal(XV)$
$$
Normal (X V) = \left( \begin{array} { l } \Phi \left( x _ { m + 1 } + v _ { 1 } \right) , \Phi \left( x _ { m + 1 } + v _ { 2 } \right) , \ldots , \Phi \left( x _ { m + 1 } + v _ { n } \right) \\ \Phi \left( x _ { m + 2 } + v _ { 1 } \right) , \Phi \left( x _ { m + 2 } + v _ { 2 } \right) , \ldots , \Phi \left( x _ { m + 2 } + v _ { n } \right) \\ \ldots \ldots \ldots \ldots \ldots \\ \ldots \ldots \ldots \ldots \ldots \\ \Phi \left( x _ { m + k } + v _ { 1 } \right) , \Phi \left( x _ { m + k } + v _ { 2 } \right) , \ldots , \Phi \left( x _ { m + k } + v _ { n } \right) \end{array} \right) _ { k \times n }
$$
>对 $Normal(XV)$ 矩阵的每一行求平均便可得到 $V_{1}$ 变量中的第 $m+1$到$m+k$ 个数据，同理，对于 $V_{2}$，我们也可以采取类似的方法 $V_{2}$ 变量中的第 $m+1$到$m+k$ 个数据。因此，通过矩阵分块思路，我们便可以将对 $V_{1}$ 和 $V_{2}$ 的求解分为为多次计算，这样电脑的运行内存便可以满足每次运算的数据量的储存要求。接下来我们对比一下循环运算和矩阵分块运算的时间。当数据量为10000时，循环运算的方法用时为113.847秒，矩阵分块运算的方法用时为6.5070秒，可以看出矩阵分块运算可以大幅缩短运算时间。

```stata
. clear all

. set obs 10000
number of observations (_N) was 0, now 10,000

. set seed 1001001

. gen x=rnormal()

. gen v=rnormal()

. *- 方法1：循环
. *------------------
. timer clear 1
. timer on 1

. local nn=10000

. gen ax =. //生成新的一列，即 ax=a[i]
(10,000 missing values generated)

. gen V1 =.
(10,000 missing values generated)

. gen V2=.
(10,000 missing values generated)

. forvalues i = 1(1)`nn'{
  2.         qui replace ax = x[`i']
  3.         gen temp`i' = normal(ax+v)
  4.         egen temp = mean(temp`i')
  5.         gen s`i' = (ax+v)*temp`i'
  6.         egen stemp = mean(s`i')
  7.         qui replace V1 = temp in `i'
  8.         qui replace V2 = stemp in `i'
  9.         drop temp temp`i' s`i' stemp
 10. }

. timer off 1
. timer list 1
   1:    113.85 /        1 =     113.8470

. *- 方法2：矩阵分块运算
. *------------------
. timer clear 2
. timer on 2

. SuperX x v

. timer off 2
. timer list 2
   2:      6.51 /        1 =       6.5070
```
&emsp;

## 5. eivprobit 的使用实例
### 5.1 蒙特卡洛模拟
>蒙特卡洛模拟是使用计算机进行重复的随机采样产生模拟概率分布的数据。本文使用的Stata的完整模拟过程及`eivprobit`命令判断结果的无偏性。
```stata
drop _all
set obs 500
set seed 1011

scalar a0 = 0
scalar a1 = 1
scalar a2 = 1
scalar a3 = -1
scalar beta = 2
scalar beta2 = -2
scalar sita = 1

gen exo = rnormal(0,2)
gen control = rnormal(-1,4)
gen control2 = control*control
gen iv1 = rnormal(0,4)
gen v1 = rnormal(0,1)
gen e1 = rnormal(0,1)

gen u1 = sita*v1+e1

gen end = 1+0.5*exo+3*iv1+v1
gen inter = end*exo

gen y1 = a0+a1*end+a2*exo+a3*inter+beta*control+beta2*control2+u1

gen y = 0 if y1<=0
replace y = 1 if y1>0

gen MPE1 = (a1+a3*exo)*normalden(y1-e1)
gen MPE2 = (a2+a3*end)*normalden(y1-e1)
gen INT = a3*normalden(y1-e1)-(a1+a3*exo)*(a2+a3*end)*(y1-e1)*normalden(y1-e1)
gen MAR = (beta+2*beta2*control)*normalden(y1-e1)
gen QUA = 2*beta2*normalden(y1-e1)-(beta+2*beta2*control)^2*(y1-e1)*normalden(y1-e1)

timer clear 1
timer on 1
eivprobit y control (end=iv1), interact(exo) quaeffect(control) ///
bootstrap(500) seed(10101)
timer off 1
//结束计时
timer list 1
```
>当样本观察值为500时，运行500次Bootstrap，运行时间为42秒，运行结果如下表所示，可以发现`eivprobit`命令估计的平均效应均是无偏的，此外我们分别计算了当样本观测值为1000，10000，20000，Bootstrap次数均为500次时候需要的时间，结果均是无偏的。

>注：测试电脑内存为7.88G，当电脑内存增大，运行时间会缩短
```stata
///样本观察值为500，运行500次Bootstrap
------------------------------------------------------------------------------
     lifesat |   True Value   Mean    Std. Err.    z      [95% Conf. Interval]
-------------+----------------------------------------------------------------
        mex1 |     0.0069    0.0087    0.0027    3.2730     0.0035     0.0140
        mex2 |    -0.0116   -0.0248    0.0102   -2.4293    -0.0448    -0.0048
      inteff |    -0.0084   -0.0159    0.0102   -1.5543    -0.0360     0.0042
       meage |     0.0434    0.0367    0.0108    3.4118     0.0156     0.0578
       quage |    -0.1745    0.0508    0.0885    0.5739    -0.1227     0.2243
------------------------------------------------------------------------------
///样本观察值为500的运行时间
. timer list 1
   1:     42.88 /        1 =      42.8790


///样本观察值为1000，运行500次Bootstrap
------------------------------------------------------------------------------
     lifesat |   True Value   Mean    Std. Err.    z      [95% Conf. Interval]
-------------+----------------------------------------------------------------
        mex1 |     0.0097    0.0104    0.0023    4.4787     0.0058     0.0149
        mex2 |    -0.0603   -0.0498    0.0070   -7.1661    -0.0635    -0.0362
      inteff |    -0.0168   -0.0198    0.0106   -1.8679    -0.0406     0.0010
       meage |     0.0125    0.0123    0.0090    1.3681     0.0053     0.0300
       quage |    -0.0010    0.0743    0.0910    0.8169    -0.1040     0.2527
------------------------------------------------------------------------------
///样本观察值为1000的运行时间
. timer list 1
   1:     62.08 /        1 =      62.0760


///样本观察值为10000，运行500次Bootstrap
------------------------------------------------------------------------------
     lifesat |   True Value   Mean    Std. Err.    z      [95% Conf. Interval]
-------------+----------------------------------------------------------------
        mex1 |     0.0072    0.0076    0.0007   10.9675     0.0062     0.0089
        mex2 |    -0.0151   -0.0145    0.0022   -6.6006    -0.0189    -0.0102
      inteff |    -0.0028   -0.0013    0.0033   -0.3775    -0.0078     0.0053
       meage |     0.0102    0.0135    0.0026    5.1636     0.0084     0.0186
       quage |    -0.0479    -0.0670   0.0282   -2.3755    -0.1222    -0.0117
------------------------------------------------------------------------------
///样本观察值为10000的运行时间
. timer list 1
   1:   2368.21 /        1 =    2368.2120


///样本观察值为20000，运行500次Bootstrap
------------------------------------------------------------------------------
     lifesat |   True Value   Mean    Std. Err.    z      [95% Conf. Interval]
-------------+----------------------------------------------------------------
        mex1 |     0.0066    0.0071    0.0005    15.3224    0.0062     0.0080
        mex2 |    -0.01378  -0.0165    0.0014   -12.1564   -0.0192    -0.0139
      inteff |    -0.0045   -0.0087    0.0022   -3.9493    -0.0130    -0.0044
       meage |     0.0151    0.0166    0.0017    9.8509     0.0133     0.0199
       quage |    -0.0296    0.0069    0.0182    0.3802    -0.0288     0.0427
------------------------------------------------------------------------------
///样本观察值为20000的运行时间
. timer list 1
   1:   8735.10 /        1 =    8735.0980

```




### 5.2 程序对比
>我们利用[Zhou (2021)](https://www.sciencedirect.com/science/article/pii/S0165176520304559?dgcid=author.pdf/)的数据前142个样本观察值，利用编写的基于循环的Bootstrap程序和`eivprobit`命令进行100次bootstrap，结果如下所示，可以发现利用`eivprobit`只需要7秒，而基于循环的Bootstrap需要13秒，对于数据量的增加，`eivprobit`对速度的增加更加显著。

```stata
. timer on 1
. eivprobit y $control (x1 = iv1 iv2 iv3 iv4), interact(x2) quaeffect(age) ///
bootstrap(100) seed(10101)
. timer off 1
//结束计时
. timer list 1
```

```stata
------------------------------------------------------------------------------
     lifesat |      Mean    Std. Err.    z     P>|z|      [95% Conf. Interval]
-------------+----------------------------------------------------------------
        mex1 |     0.0205    0.0528    0.3885   0.6997     -0.0829     0.1240
        mex2 |    -1.4045    0.8771   -1.6014   0.1093     -3.1236     0.3146
      inteff |    -0.0708    0.0702   -1.0077   0.3163     -0.2085     0.0609
       meage |    -0.1183    0.0741   -1.5970   0.1103     -0.2636     0.0269
       quage |     0.0485    0.0566    0.8572   0.3913     -0.0624     0.1595
------------------------------------------------------------------------------

. timer off 1

. //结束计时
. timer list 1
   1:      7.23 /        1 =       7.2310
```

```stata
///利用循环的平均效应计算
------------------------------------------------------------------------------
     lifesat |      Mean    Std. Err.    z     P>|z|      [95% Conf. Interval]
-------------+----------------------------------------------------------------
        mex1 |     0.0205    0.0528    0.3885   0.6997     -0.0829     0.1240
        mex2 |    -1.4045    0.8771   -1.6014   0.1093     -3.1236     0.3146
      inteff |    -0.0708    0.0702   -1.0077   0.3163     -0.2085     0.0609
       meage |    -0.1183    0.0741   -1.5970   0.1103     -0.2636     0.0269
       quage |     0.0485    0.0566    0.8572   0.3913     -0.0624     0.1595
------------------------------------------------------------------------------

. timer off 1

. //结束计时
. timer list 1 
   1:     13.99 /        1 =      13.9870
```



### 5.3 运用实例
>这里,我们以[Zhou (2021)](https://www.sciencedirect.com/science/article/pii/S0165176520304559?dgcid=author.pdf/)的数据,结合`eivprobit`命令来估计家庭社交网络 $social_{i}$ 和 $APP-Internet$ 使用量对家庭是否参加风险投资的平均边际效应, 交互效应及户主年龄对家庭是否参加风险投资的平均边际效应,平均平方效应. 

>[Zhou (2021)](https://www.sciencedirect.com/science/article/pii/S0165176520304559?dgcid=author.pdf/) 利用`Probit`回归得到家庭使用金融经济软件或者互联网获得信息的概率刻画 变量 `APP-Interet`. [Zhou (2021)](https://www.sciencedirect.com/science/article/pii/S0165176520304559?dgcid=author.pdf/)认为[Chen and Ji (2017)](https://www.tandfonline.com/doi/full/10.1080/1540496X.2016.1263794)利用家庭在婚礼和葬礼上赠与和被赠与的钱作为家庭社交网络 $social_{i}$ 的代理变量,在刻画家庭是否参加风险投资中存在内生性问题,因此作者利用家庭是否使用手机 $cell_{i}$ , 是否网上购物 $online_{i}$, 家庭每月平均水,电,燃气等成本 $wefee_{i}$作为 $social_{i}$ 的工具变量.

>变量生成的具体代码如下所示:


```Stata
. global control lnwealth lnincome age age2 edu gender marriage risk_lover ///
risk_averter size job rural east west 

//获得变量 APP-Internet
. probit inf1 iphone onlineshop cell we_fee $control
. predict inf1hat,xb
. gen inf=normal(inf1hat) 

//将家庭是否参加风险投资转为0-1变量
. gen y=1 if rate_r>0 & rate_r!=.
. replace y=0 if rate_r==0  


. gen x1=lnsocial     //内生变量
. gen x2=inf
. gen x1x2=x1*x2      //交乘项

//定义家庭社交网络的工具变量
. gen iv1=iphone  
. gen iv2=onlineshop
. gen iv3=cell
. gen iv4=we_fee

//由于利用quaeffect选项会自动生成age的平方项,故不需要在下面放入
. global control lnwealth lnincome age ///
       edu gender marriage risk_lover risk_averter size job rural east west 
```
>本部分将分析两个主要解释变量的平均边际效应,交乘项的平均交互效应,户主年龄的平均边际效应和平均平方效应,命令如下,其中 `mex1` , `mex2` 分别为家庭社交关系,家庭`APP-Internet`指数对家庭风险投资的平均边际效应,可看到显著为正,但两者交互项的平均交叉效应 `inteff` 显著为负. 对于户主年龄 `age` 的平均边际效应 `meage`, 其显著为负;而其平均平方效应 `quage` 显著为负.

```Stata
. time clear 1
. time on 1
. eivprobit y $control (x1 = iv1 iv2 iv3 iv4 ), interact(x2) ///
quaeffect(age) bootstrap(500) seed(10101)

(option xb assumed; fitted values)
(option pr assumed; Pr(y))
(0 observations deleted)
(0 observations deleted)
(37,794 real changes made)
(37,794 real changes made)
(37,794 real changes made)
run=1
(37,794 real changes made)
(37,794 real changes made)
(37,794 real changes made)
run=2
(37,794 real changes made)
(37,794 real changes made)
(37,794 real changes made)
......
run=499
(37,794 real changes made)
(37,794 real changes made)
(37,794 real changes made)
run=500
(37,794 real changes made)
(37,794 real changes made)
(37,794 real changes made)
(37,744 real changes made, 37,744 to missing)
(37,744 real changes made, 37,744 to missing)
(37,744 real changes made, 37,744 to missing)
(37,744 real changes made, 37,744 to missing)
(37,744 real changes made, 37,744 to missing)

------------------------------------------------------------------------------
     lifesat |      Mean    Std. Err.    z     P>|z|      [95% Conf. Interval]
-------------+----------------------------------------------------------------
        mex1 |     0.0554    0.0069    8.0563   0.0000      0.0419     0.0689
        mex2 |     0.1928    0.0600    3.2143   0.0013      0.0752     0.3103
      inteff |     0.0480    0.0064    7.5185   0.0000      0.0355     0.0605
       meage |     0.0068    0.0018    3.8297   0.0001      0.0033     0.0102
       quage |    -0.0077    0.0023   -3.3551   0.0008     -0.0122    -0.0032
------------------------------------------------------------------------------

. timer off 1

. //结束计时
. timer list 1
   1:  85081.51 /        1 =   85081.5070

. 
end of do-file

```



&emsp;

## 6. 参考资料

> Note：如下内容可以使用 `lianxh 内生非线性模型的平均效应, m` 命令自动生成。执行 `ssc install lianxh` 可以下载 `lianxh` 命令。 

- Ai, C., Norton, E.C., 2003. Interaction terms in logit and probit models[J]. Econom. Lett. 80, 123-129. [-PDF-](https://www.sciencedirect.com/science/article/abs/pii/S0165176503000326)

- Richard, P., 2019. Residual bootstrap tests in linear models with many regressors[J]. J. Econometrics 2008, 367-394. [-PDF-](https://www.sciencedirect.com/science/article/abs/pii/S0304407618301921)

- Pinar Karaca-Mandic, 2011. Edward C. Norton, and Bryan Dwod. Interaction Terms in Nonlinear Models[J]. Health Services Research, 47: 255-74. [-PDF-](https://www.researchgate.net/publication/51809066_Interaction_Terms_in_Non-Linear_Models)

- Xianbo Zhou, Heyang Li, 2021. Interaction and quadratic effects in probit model with endogenous regressors[J]. Economics Letters 198: 109695. [-PDF-](https://www.sciencedirect.com/science/article/abs/pii/S0165176520304559?via%3Dihub)

- Baum. An Introduction to Stata Programming[M]. 2016
- Gould. Mata Book New[M]. 2018
- Wooldridge, J.M., 2002. Econometric Analysis of Cross Section and Panel Data.  MIT Press  [-PDF-](https://books.google.com/books?hl=zh-CN&lr=&id=hSs3AgAAQBAJ&oi=fnd&pg=PP1&ots=VYMQoBYXOp&sig=5XKvvKVqAUaYEilDgfv2KoNjwIU#v=onepage&q&f=false)

- 钟经樊 连玉君. 计量分析与 STATA 应用[M]. 2008

## 7. 相关推文

> Note：产生如下推文列表的命令为：   
> &emsp; `lianxh logit probit, m`  
> 安装最新版 `lianxh` 命令：    
> &emsp; `ssc install lianxh, replace` 

- 专题：[Probit-Logit](https://www.lianxh.cn/blogs/27.html)
  - [Stata：多元 Logit 模型详解 (mlogit)](https://www.lianxh.cn/news/270f2c9e75d4a.html)
  - [Stata：Logit模型一文读懂](https://www.lianxh.cn/news/e18031ffad4f3.html)
  - [详解 Logit/Probit 模型中的 completely determined 问题](https://www.lianxh.cn/news/a03aa1b7e32e0.html)
  - [Stata：Logit 模型评介](https://www.lianxh.cn/news/011888bd49e07.html)
  - [二元选择模型：Probit 还是 Logit？](https://www.lianxh.cn/news/979079951adf2.html)
  - [Stata：何时使用线性概率模型而非Logit？](https://www.lianxh.cn/news/7a44186e16de7.html)
  - [Stata：嵌套 Logit 模型 (Nested Logit)](https://www.lianxh.cn/news/d5e00bfb17a7c.html)
  - [Stata：二元Probit模型](https://www.lianxh.cn/news/8b0815c8c1116.html)
  - [动态 Probit 模型及 Stata 实现](https://www.lianxh.cn/news/9b7224e2808ff.html)
  - [Stata新命令：面板-LogitFE-ProbitFE](https://www.lianxh.cn/news/18a8416f25cce.html)
- 专题：[交乘项-调节](https://www.lianxh.cn/blogs/21.html)
  - [内生变量的交乘项如何处理？](https://www.lianxh.cn/news/1f6e3f51c4991.html)
  - [interactplot：图示交乘项-交互项-调节效应](https://www.lianxh.cn/news/22d2b83df9cac.html)
  - [Stata：交乘项的对称效应与图示](https://www.lianxh.cn/news/6b16065e2781d.html)
  - [Stata：图示交互效应-调节效应](https://www.lianxh.cn/news/29ab818725ff0.html)
  - [Stata：交乘项该如何使用？-黄河泉老师PPT](https://www.lianxh.cn/news/71dfcc01f5581.html)
  - [Stata：虚拟变量交乘项生成和检验的简便方法](https://www.lianxh.cn/news/e393be31ef177.html)
  - [Stata：图示连续变量的连续边际效应](https://www.lianxh.cn/news/a71535e4c99f0.html)
  - [Stata：内生变量和它的交乘项](https://www.lianxh.cn/news/9056df610bb6c.html)
  - [Stata：交乘项该这么分析!](https://www.lianxh.cn/news/c42dc033a0999.html)
  专题：[回归分析](https://www.lianxh.cn/blogs/32.html)
  - [Stata: 边际效应分析](https://www.lianxh.cn/news/3e47d865073a6.html)
  - [Stata: 手动计算和图示边际效应](https://zhuanlan.zhihu.com/p/73944131)