<font size=5>**Stata中的 RIF 回归和 RIF 分解：探究收入不平等性的成因**</font>

&emsp;

> **作者**：张翊 (中山大学)    
> **E-Mail:** <zhangy779@mail2.sysu.edu.cn>  
> **Source:** Fernando Rios-Avila, 2020, Recentered influence functions (RIFS) in Stata: RIF regression and RIF decomposition. [-PDF-](https://sci-hub.do/10.1177/1536867X20909690)

&emsp;

---

**目录**
[TOC]

---

&emsp;

本文在 Fernando (2020) 基础上，介绍在 Stata 中应用 RIF 回归和 RIF 分解的方法，以及 `rifvar` 、`rifhdreg` 、`oaxaca_rif ` 三个命令。

**RIF(Recentered influence function)** 的中文名字是“再中心化影响函数”，用于衡量样本中某一处微小变化对统计量的影响。

RIF 回归和 RIF 分解方法主要应用于劳动经济学与经济政策评估中，可用于探究分配不平等性、代际差异的构成因素。

## 1. 背景介绍

以下是 1998 年瑞士劳动力市场的一份职工收入数据。我们如何判断该市场上的收入分配不平等的情况？

    . use http://fmwww.bc.edu/RePEc/bocode/o/oaxaca.dta, clear
    
    . list lnwage educ exper tenure female in 1/10
    
           +-----------------------------------------------+
           |  lnwage   educ      exper     tenure   female |
           |-----------------------------------------------|
        1. |  3.73304      9   9.041667   3.416667       1 |
        2. | 3.600868      9       7.25   20.66667       0 |
        3. | 3.159036   10.5        2.5   .0833333       1 |
        4. | 3.393229     12       26.5   6.166667       0 |
        5. | 3.805663     12   13.91667   1.333333       0 |
        6. | 3.272365   10.5       9.25          0       0 |
        7. | 3.528298   10.5   29.41667      28.75       0 |
        8. | 3.805663   17.5   10.08333       6.25       0 |
        9. | 3.824012   17.5   2.041667   5.083333       0 |
        10.| 3.382179   10.5   10.83333   8.583333       0 |
           +-----------------------------------------------+

**首先，我们可以研究整体工资收入的分布情况**：使用方差、分位距等统计量，可以计算出全体职工收入分配的不均衡程度。

    . qui sum lnwage, detail
    
    . scalar variance = r(Var)
    
    . disp variance
    .28200963
    
    . scalar iqr90_10 = r(p90)-r(p10)
    
    . disp iqr90_10
    1.0533128

**其次，还可以探究男女收入分配的差异性**：样本中男职工 **lnwage** 均值为 3.440，女职工 **lnwage** 均值为 3.267，差距为 0.173。

通过 Oaxaca-Blinder 分解，我们可以进一步将此差距拆分为：

**<font color = #53282b>· 特征效应</font>**: 可解释的部分，即因为女性的平均教育水平、工作年限特征低于男性，而使得女性的平均工资水平较低的部分<br/>
**<font color = #53282b>· 系数效应</font>**: 不可解释的部分，即在男女性的教育水平、工作年限等特征相同的情况下，男性平均工资比女性平均工资高的部分

<font size = 2>(注：关于 Oaxaca-Blinder 分解的详细说明，可参见连享会推文 [性别收入差距=歧视？Oaxaca-Blinder分解方法](https://www.lianxh.cn/news/628d565acfb49.html) ) </font>

然而，以上的分析仅停留在“截面”的层面，没有对工资收入分布的情况做进一步的分析。

使用 Fernando (2020) 提供的 `rifhdreg` 、`oaxaca_rif` 命令，我们能够进一步探究如下问题：

**<font color = #b30512>· 是什么导致了整体收入分配的不均衡？</font>** 也就是说，哪些特征的变化能够降低收入分配的不平等现象？

**<font color = #b30512>· 男女职工内部的收入分配是否存在不均衡？如果存在，是由什么引起的？</font>** 也就是说，在均值差异的基础上，女性工资收入的两极分化是否也比男性更严重？这种不平等性的差异是什么造成的？

&emsp;

## 2. RIF是什么？

在探究上述问题之前，我们首先要认识一下本文的主角：RIF.

**RIF(Recentered influence function)** 的中文名字是“再中心化影响函数”，用于衡量样本中某一处微小变化对统计量的影响。

我们知道，计算这些统计量依赖于样本分布，而样本中的变化正是通过影响样本分布，进而影响统计量的。

比如， $x$ 的原始分布 $F(x)$ 为正态分布。此时均值 $x=0$ 处的样本增加了，导致 $x$ 的样本分布变为 $G(x)$ :

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/张翊_RIF回归和RIF分解_Fig1.png)


可见，在 $x$ 的分布发生改变后，相应的方差、分位数等统计量也会发生相应的改变 (由于本例中的 $x$ 在均值处增加，因此均值统计量不变) 。

如何衡量 $x$ 的微小变化对其整体统计量的影响呢？RIF 的计算基于 IF (Influence function) ，是在 IF 基础上加上原始的统计量构成的。具体的定义式如下：

$$
\textstyle IF\{y_i,v(F_y)\}=
\lim\limits_{ε \to 0} \frac{v\{(1-ε)F_y+εH_{y_i}\}-v(F_y)}{\varepsilon}=\frac{\partial F_y \to H_{y_i}}{\partial \varepsilon}
$$

$$
\textstyle RIF\{y_i,v(F_y)\}=IF\{y_i,v(F_y)\}+v(F_y)
$$

其中，$F_y$是 $y$ 的原始分布，$H_{y_i}$ 是仅在 $y_i$ 处取值的一个分布，$v(F_y)$ 即为相应的统计量，如均值、方差等。

IF 的构造能够量化 $y_i$ 的“微小变化”：在 $F_y$ 的基础上，向仅在 $y_i$ 处取值的分布变动 $\varepsilon $ 个单位。取极限后得到 IF ，其涵义为：$y=y_i$ 的样本发生微小的增加后，其统计量将发生的改变。

RIF 在 IF 的基础上，加上了在原分布中的相应统计量，其涵义为：考虑了 $y_i$ 处的影响后，统计量 $v$ 的近似值。可见， RIF 的决定因素是原始分布 $F_y$ 、变化位置 $y_i$ 和选取的统计量 $v$ ，在给定原始分布和统计量时，RIF 是 $y$ 的函数。

Fernando (2020) 提供的 `rifvar( )` 函数用于计算许多统计量的 RIF 值。`rifvar( )` 内嵌于 `egen` 函数，使用时需指明对应的变量和统计量。具体使用方法如下：

    . egen rif_mean = rifvar(lnwage), mean //lnwage均值的RIF
    
    . egen rif_var = rifvar(lnwage), var //lnwage方差的RIF
    
    . egen rif_iqr = rifvar(lnwage), iqr(10 90) //lnwage的90-10分位距的RIF
    
    . egen rif_gini = rifvar(lnwage), gini //lnwage基尼系数的RIF
    
    . list lnwage rif_mean rif_var rif_iqr rif_gini in 1/5
    
       +----------------------------------------------------------+
       |   lnwage    rif_mean     rif_var     rif_iqr    rif_gini |
       |----------------------------------------------------------|
    1. | 3.73304    3.7330403   .14095217   .50356058   .05171443 |
    2. | 3.600868   3.6008685   .05917743   .50356058   .0343611  |
    3. | 3.159036   3.1590359   .03942942   .50356058   .05009091 |
    4. | 3.393229   3.3932292   .00126913   .50356058   .02673849 |
    5. | 3.805663   3.8056629   .20075643   .50356058   .06402844 |
       +----------------------------------------------------------+


使用 `rifvar( )` 函数，可以计算 RIF 的统计量包括均值、方差、分位数、基尼系数等。其语法结构如下：

     egen [type] newvar = rifvar(varname) [if] [in]  ///
      [, by(varname) weight(varname) seed(str) RIF_options]

- `newvar`：生成变量名称；
- `varname`：用于计算RIF的变量；
- `by(varname)`：分组变量，如性别、种族等；
- `weight(varname)`：权重变量；
- `seed(str)`：指明复制等级依赖指数 (rank-dependent indices) 的特定种子值；
- `RIF_options`：指出计算的RIF统计量，能计算的统计量及相应命令可在 help 文档中查阅；


&emsp;

## 3. RIF回归：什么导致了收入分配不平等？

### 3.1 标准RIF回归

在背景介绍部分的例子中，我们想要探究如下问题：职工工资的分配不平等是由什么造成的？同时，什么因素能够改善这种不平等？

如果我们有如下含有基尼系数和相对应的平均教育年限、平均工作经验等特征的样本，便可以使用 OLS 解决这一问题：

    . list gini edu_m exper_m tenure_m in 1/5
    
       +-----------------------------------+
       | gini   edu_m   exper_m   tenure_m |
       |-----------------------------------|
    1. |  .47       1       2.5          3 |
    2. |  .35       2       3.5          4 |
    3. |  .56       3       2.5          2 |
    4. |   .2       4         6          3 |
    5. |  .89       5       4.5          1 |
       +-----------------------------------+
    
    . reg gini edu_m exper_m tenure_m

然而，需要多个截面数据 (如多个国家不同时点的数据) 才能得到较大的样本，且样本的来源混杂也让结果失去针对性，只能反映一个较大的总体的情况。

如果我们想研究某一国家、某一时间段的不平等性，则只能获得如背景介绍中的数据：

       +-----------------------------------------------+
       |  lnwage   educ      exper     tenure   female |
       |-----------------------------------------------|
    1. | 3.73304       9   9.041667   3.416667       1 |
    2. | 3.600868      9       7.25   20.66667       0 |
    3. | 3.159036   10.5        2.5   .0833333       1 |
    4. | 3.393229     12       26.5   6.166667       0 |
    5. | 3.805663     12   13.91667   1.333333       0 |
       +-----------------------------------------------+

使用这个数据只能算出一个基尼系数，无法反映其变化。同时，若选取工资作为被解释变量时，只能得到教育水平、工作年限等提高一个单位时工资的变化，而无法直观地反映不平等性相关的问题。此时，RIF 回归派上了用场。

RIF具有一条重要的性质：

$$
\textstyle \int RIF\{y_i,v(F_y)\}=v(F_y)
$$

也就是说，RIF 的无条件期望就是相应的统计量本身。这一性质为 RIF 回归奠定基础。

将 RIF 作为被解释变量，进行 OLS 回归：

$$
\textstyle RIF\{y_i,v(F_y)\}=\mathbf X '\beta+ε_i, E[ε_i]=0
$$

在上式左右两侧取无条件期望。由于 RIF 的无条件期望即为相应统计量，我们能够得到：

$$
\textstyle v(F_y)=E[RIF\{y_,v(F_y)\}]=E[ X '\beta]+E[ε_i]=\overline  X'\beta
$$

此时，$\beta_k$的涵义 (UPE, unconditional partial effect) 如下：控制其他条件不变，**总体中 $x_k$ 的均值**提高一个单位时，$y$ 的统计量 $v$ 将提高 $\beta_k$。

当统计量 $v$ 选为分位距、基尼系数等不平等指标时，便可反映 $x_k$ 的总体均值变化对总体不平等性的影响。

### 3.2 标准RIF回归：操作实例

使用背景介绍中的职工收入数据，通过 RIF 回归探究工资收入不平等现象及其影响因素。

Fernando (2020) 提供的 `rifhdreg` 命令，可以方便地进行RIF回归。使用方法如下：
    

    . use http://fmwww.bc.edu/RePEc/bocode/o/oaxaca.dta, clear
    
    . gen wage=exp(lnwage)
    
    . drop if lnwage==.
    
    . rifhdreg lnwage educ exper tenure female, rif(iqr(90 10)) ///
     vce(robust) abs(age isco single) 
    
    . rifhdreg wage educ exper tenure female rif(iqratio(90 10)) ///
     vce(robust) abs(age isco single) 
    
    . rifhdreg wage educ exper tenure female, rif(gini) ///
     vce(robust) abs(age isco single) 
    
    . rifhdreg wage educ exper tenure female, rif(var) ///
     vce(robust) abs(age isco single)

其中，`rif()` 选项指明了要计算的统计量；`vce(robust)` 选项表示使用稳健的标准误；与 `reghdfe` 命令类似，`abs()` 选项能够控制多维的固定效应，此处控制了年龄、职业和是否单身的固定效应。

回归结果如下。为方便解读，此处在第一行中加上了对应的 RIF 统计量，并在末尾加入了每个回归中 RIF 的平均值。由于 RIF 的无条件期望等于对应的统计量，此处 RIF 均值 (期望) 即为样本的对应统计量 (基尼系数、方差等) 。

      -------------------------------------------------------------------
                  (1)             (2)             (3)             (4)   
                 lnwage           wage            wage           wage   
               iqr(90 10)     iqratio(90 10)      Gini         Variance
      -------------------------------------------------------------------
      educ      0.0280*         0.0772          0.00719**       36.30***
               (0.0164)        (0.0479)        (0.00366)        (11.77)   
      exper    -0.0215***      -0.0632***      -0.00331***      -7.273   
               (0.00467)       (0.0136)        (0.00117)        (5.367)   
      tenure    0.000580        0.00103        -0.000476        -0.357   
               (0.00466)       (0.0137)        (0.00112)        (5.809)   
      female    0.0790          0.249           0.0355***       71.03   
               (0.0627)        (0.184)         (0.0119)         (44.49)   
      -------------------------------------------------------------------
      Avg.RIF    1.0595          2.8844          0.2460          259.83
      N           1434            1434            1434            1434   
      -------------------------------------------------------------------
      Standard errors in parentheses
      * p<0.1, ** p<0.05, *** p<0.01

我们可以根据RIF回归解读不平等性的影响因素。

- **ln(wage)** 的 90-10 分位距值为 1.0595，相应回归中 **edu** 的系数为 0.0280，说明样本中所有职工的平均受教育年限提高 1 个单位 (此处为 1 年) 时，**ln(wage)** 的 90 分位数与 10 分位数的差值将增大 0.0280，即增大2.9%。
- **wage** 的基尼系数值为 0.2460，相应回归中 **exper** 的系数为 -0.0033，说明样本中所有职工平均工作经验提高 1 年，**wage** 的基尼系数将减少 0.0033，即减少 1.3%。

这说明，该样本所处的劳动力市场总体中，平均教育年限的增加将增大收入分配的不平等，而平均工作经验的增加将降低收入分配的不平等。

### 3.3 RIF回归：分组处理效应

我们知道，对类别变量 (如：男性 = 0，女性 = 1) 进行 OLS 回归可以探究不同组别在解释变量中的差异。

在RIF回归中同样可以使用类似的方法研究分组处理效应，通过样本的回归探究整体中不同组别的分布差异，尤其是分布不平等性的差异。

例如，我们想基于上述瑞士劳动力市场样本数据，探究所反映的整体中男女性收入不平等性情况。定义分组变量 $T$，当样本为女性时 $T=1$，男性则 $T=0$. 以下讨论中，$Y$ 为工资收入，$X$ 为教育年限、工作经验等个体特征。

此处有一个重要假设——**非混淆假设 (Unconfoundedness Assumption)**：

控制个体特征X后，男女性的工资收入 $Y_1$、$Y_0$ 与“性别分组” $T$ 的“分组方法”独立，即分组是尽可能随机的。也就是说，可以由一个未提前设定性别的样本出发，通过分组 (指定谁为男性、女性，再去工作获得工资) ，得到 $F_{Y1}$、$F_{Y0}$ 两个样本：

$$
\textstyle F_{Y_k}=\int F_{Y_k|X,T}dF_{X,T} = \int F_{Y_k|X}dF_{X}
$$

但是，我们能观察到的是已经进行“性别分组”后的男性、女性两个分布：

$$
\textstyle F_{Y_k|T=k}=\int_{i∈k} F_{Y_k|X}dF_{X|T=k}\quad for\quad k=0,1
$$

我们只能观察到男性、女性在“有性别”后的特征分布 $dF_{X|T=k}$，而不能观察到未分组 (“无性别”) 的特征 $dF_X$. 在预设性别分组后，$X$ 可能会受到 $T$ 的影响，如某些职工可能因为性别为女性，使得受教育年限较短。因此，我们不能直接获得 “未预设性别”、取得数据后再进行性别分组的分布 $F_{Y_k}$.

然而，我们可以通过能观察到的特征分布 $dF_{X|T=k}$，乘以相应的权重，得到 $dF_X$ 的拟合值，进而得到 $F_{Y_k}$ 的拟合值：

$$
\hat{F}_{Y_k}=\int_{i∈k} F_{Y_k|X}w_k(\mathbf x)dF_{X|T=k}
$$

根据贝叶斯公式，权重 $w_k(\mathbf x)$ 的计算方法如下：

$$
w_k(\mathbf x)={dF_X\over dF_{X|T=k}}=dF_X \times  {P(T=k)\over P(T=k|X=x)dF_X} = {P(T=k)\over P(T=k|X=x)}
$$

得到 $T=1$、$T=0$ 两个组别的权重：
$$
w_1(\mathbf x)={P(T=1)\over P(T=1|X=x)}\\
w_0(\mathbf x)={1-P(T=1)\over 1-P(T=1|X=x)}
$$

分子 $P(T=1)$ 即样本中女性的比例，分母则是给定一系列特征后，观测值为女性的概率。分子中的这一概率可以通过 Logit 、Probit 等模型估计得到。由此可得到权重 $w_k(\mathbf x)$，进而得到 $F_{Y_k}$ 的拟合值。

上述过程称为 **分配权重调整 (reweighting adjustment)** 。可以使用这一拟合的分布，计算对应组别的 RIF 。接下来进行RIF回归：

$$
T \times RIF\{y,v(\hat{F}_{Y_1})\}+(1-T)\times RIF\{y,v(\hat{F}_{Y_0})\}=b_0+b_1T+\varepsilon
$$

被解释变量由两个组别的 RIF 合成，根据 Fernando (2020) ，对 $T$ 进行 WLS 回归，权重为 $\hat{w}(\mathbf x)= T \times \hat{w_1}(\mathbf x)+(1-T) \times \hat{w_0}(\mathbf x)$. 等式两侧对 $T$ 取无条件期望，系数 $b_1$ 即为两组别的统计量之差：

$$
T \times v(\hat{F}_{Y_1})+(1-T)\times v(\hat{F}_{Y_0})=b_0+b_1T+\varepsilon
\\
v(\hat{F}_{Y_1}) - v(\hat{F}_{Y_0})=b_1
$$

我们可能会有一个疑问：既然能得到拟合出的分布了，为什么不直接相减得到不平等性的差异呢？

在这里，使用 RIF 回归的好处在于，可以得到“控制其他条件不变”时的结果。即：如果男女性的受教育年限、工作经验等特征相同，其收入分配不平等性的差异有多大。

### 3.4 RIF 回归：分组处理效应操作实例

继续使用上文瑞士劳动力市场数据，探究样本反映的整体中男女性收入分配不平等性的差异。

回归过程如下。

    . global xvar educ exper tenure age single
    
    . local rif iqr(90 10)
    . rifhdreg lnwage female $xvar, over(female) rif(`rif') robust
    . rifhdreg lnwage female $xvar, ///
      over(female) rif(`rif') robust rwprobit($xvar)
      
    . local rif gini
    . rifhdreg wage female $xvar, over(female) rif(`rif') robust
    . rifhdreg wage female $xvar, ///
      over(female) rif(`rif') robust rwprobit($xvar)
    
    . local rif var
    . rifhdreg lnwage female $xvar, over(female) rif(`rif') robust
    . rifhdreg lnwage female $xvar, ///
      over(female) rif(`rif') robust rwprobit($xvar)

其中，每个统计量回归的第一列未进行分配权重调整，直接采用可观察到的男女性样本分布计算 RIF 、进行回归，具有一定的误差。第二列使用 Probit 模型进行分配权重调整后，实现方法为加入`rwprobit()` 选项，且 Probit 回归的解释变量即为RIF回归的控制变量。

还可以通过 `rwlogit()` 选项采用 Logit 回归获得权重，或通过 `rwmprobit()`、`rwmlogit()` 采用多项式的 Probit、Logit 回归。

回归两列结果均加入了 **edu**、**exper**、**tenure** 等控制变量，且采用稳健的标准误。

结果如下所示：

   --------------------------------------------------------------------
                  (1)             (2)             (3)             (4)   
               lnwage          lnwage            wage            wage   
             iqr(90 10)      iqr(90 10)          Gini            Gini
    --------------------------------------------------------------------
    female     0.0710          0.0973*         0.0345***       0.0371***
              (0.0572)        (0.0588)        (0.0133)        (0.0132)   
    educ     -0.00429        -0.00360        -0.00551*       -0.00555*  
              (0.0138)        (0.0156)       (0.00328)       (0.00325)   
    exper     -0.0211***      -0.0152***     -0.00463***     -0.00346***
             (0.00413)       (0.00429)       (0.00126)      (0.000922)   
    tenure    0.00478         0.00887*       0.000543        0.000656   
             (0.00429)       (0.00537)       (0.00100)      (0.000906)   
    age       0.00862*        0.00247         0.00150       -0.000193   
             (0.00458)       (0.00437)       (0.00157)       (0.00123)   
    single     0.0621           0.172**       0.00975         0.00910   
              (0.0578)        (0.0751)        (0.0145)        (0.0163)   
    _cons       0.901***        0.971***        0.285***        0.331***
              (0.207)         (0.238)        (0.0506)        (0.0549)   
    --------------------------------------------------------------------
    N            1434            1434            1434            1434   
    --------------------------------------------------------------------


可见，女性工资对数的 90-10 分位距、基尼系数均大于男性，说明控制其他因素不变时，女性工资收入的不平等程度仍然更高。

`rifhdreg` 的语法结构如下：

    rifhdreg depvar [indepvars] [if] [in] [weight], ///
    rif(RIF_options) [retain(newvar) replace ///
                abs(varlist) iseed(str) over(varname)     ///
                rwlogit(varlist) rwprobit(varlist)        ///
                rwmlogit(varlist) rwmprobit(varlist)      ///
                 [ate|att|atu] scale(real) svy            ///
                regress_options reghdfe_options]

- `depvar`：被解释变量；
- `indepvar`：解释变量；
- `rif(RIF_options)`：指出计算的 RIF 统计量，命令与 `rifvar` 中相同；
- `retain(newvar)`：保存计算得到的 RIF ；
- `abs(varlist)`：指明控制固定效应的变量，可放多个变量，用法与 `reghdfe` 命令中相同；
- `iseed(str)`：指明复制等级依赖指数 (rank-dependent indices) 的特定种子值；
- `over(varname)`：RIF分组组别，可理解为该变量的部分条件 RIF  (partial conditional RIF) ，当该变量为二值变量时，RIF 回归可视为 Oaxaca-Blinder 分解的 OLS 替代；
- `rwlogit(varlist), rwprobit(varlist), rwmlogit(varlist) rwmprobit(varlist)`：分组处理效应中，指明获取 RIF 权重的方法，分别为 Logit / Probit / Mlogit / Mprobit；
- `ate|att|atu`：分组处理效应中，指出要求的处理效应，分别为整个样本平均处理效应 (默认值，ate)， 处理组平均处理效应 (att)， 控制组平均处理效应 (atu) ;
- `scale(real)`：被解释变量的扩大倍数，例如可将基尼系数扩大100倍；
- `svy`：使用调查设计 (survey design) 方法得到估计值，不可与固定效应同时使用；
- `regress_options, reghdfe_options`：使用 `abs((varlist)` 选项时，可采用 `reg` 和 `reghdfe` 命令的所有选项


&emsp;
## 4. 进阶拓展：RIF 分解

### 4.1 RIF 分解介绍

在通过上文的方法获得控制其他因素不变时男女性收入不平等性的差异后，我们还可以进一步探究背景介绍中的问题：是什么导致了这一差异？使用 Oaxaca-Blinder 分解，我们能够得到男女性收入均值的特征效应 (可解释部分) 和系数效应 (不可解释部分) 。通过 RIF 回归，我们将能够把男女性收入的不平等性拆分为这两部分。

借鉴 OB 分解，结合 RIF 回归的性质，我们可以得到：

$$
v_1=E[RIF(y,v(F_{Y|T=1}))]=\overline   X^{1'}\hat{\beta}^{1}\\
v_0=E[RIF(y,v(F_{Y|T=0}))]=\overline   X^{0'}\hat{\beta}^{0}\\
v_c=\overline   X^{1'}\hat{\beta}^{0}
$$

其中 $v_c$ 为反事实组的统计量 $v$ ，即具有女性特征  ($\mathbf  X^{1}$) 而被当成男性 ($\hat{\beta}^{0}$) 的虚构职工。然而使用如上的线性方式构造 $v_c$ 时，如果因为遗漏变量等原因导致模型设定存在偏误， $v_c$ 的构造将不准确。

RIF 分解中，采用与上一节相似的分配权重调整 (reweighting adjustment) 方法，更精确地构造出“被当成男性的女性职工”工资分布拟合值：

$$
F_Y^C=\int F_{Y|X,T=0}dF_{X|T=1}\cong \int F_{Y|X,T=0}dF_{X|T=0}w(\mathbf X)
$$

其中，权重 $w(\mathbf X)$ 也可由 Logit、Probit 等模型获得，$T=0$ 组别的 $\mathbf X$ 乘上权重后便得到 $\mathbf X_C$。基于此，进行 RIF 回归得到系数估计值：

$$
v_c=E[RIF(y,v(F_Y^C))]=\overline  X^{C'}\hat{\beta}^{C}
$$

RIF 分解过程如下：

$$
\Delta v = \underbrace{{\overline  X^{1}}(\hat{\beta_1}-\hat{\beta_C})}_{\Delta v_s^p}+
\underbrace{({\overline X^{1}-\overline  X^{C})}\hat{\beta_C}}_{\Delta v_s^e}+
\underbrace{({\overline  X^{C}-\overline  X^{0})}\hat{\beta_0}}_{\Delta v_x^p}+
\underbrace{{\overline X^{C}}(\hat{\beta_C}-\hat{\beta_0})}_{\Delta v_x^e}
$$
分解得到的四项中，第一、二项之和为系数效应，第三、四项之和为特征效应。

其中，第一项是纯系数效应，第二项是由于分配权重调整造成的误差，大样本中应趋于 0 ；第三项是纯特征效应，第四项是由于模型设定造成的误差。也就是说：

   **· 可解释部分——特征效应 (Total composition effect) =** 

纯特征效应(Pure composition effect) + 模型设定误差 (Specification error) 

   **· 不可解释部分——系数效应 (Total wage structure) =** 

纯系数效应(Pure wage structure) + 权重分配误差 (Reweighting error) 

### 4.2 RIF 分解：操作实例

继续使用瑞士工资收入数据，探究男女性收入不平等性的来源；采用 90-10 分位距、基尼系数和方差作为不平等衡量指标，并使用 Logit 回归获得权重值。分解过程如下：

    . oaxaca_rif lnwage educ exper tenure, ///
      rif(iqr(90 10)) by(female) rwlogit(educ exper tenure)
    
    . oaxaca_rif wage educ exper tenure, ///
      rif(gini) by(female) rwlogit(educ exper tenure)
    
    . oaxaca_rif lnwage educ exper tenure, ///
      rif(var) by(female) rwlogit(educ exper tenure)

分解结果如下。此处为方便理解，在原输出结果基础上加入了注解：特征效应 (Composition effect) 即为可解释部分 (Explained) ，系数效应 (Wage structure effect) 即为不可解释部分 (Unexplained) .

      ---------------------------------------------------------------------
                                  (1)            (2)             (3)   
                                 lnwage          wage           lnwage   
                               iqr(90 10)        Gini          Variance
      ---------------------------------------------------------------------
      Overall                                                   
      Group_1(Male)             0.952***       0.221***         0.229***
                               (0.0331)       (0.00676)        (0.0216)   
      Group_c(Counterfactual)   1.083***       0.259***        0.285***
                               (0.0473)        (0.0118)        (0.0269)   
      Group_2(Female)           1.062***       0.267***        0.324***
                               (0.0432)        (0.0117)        (0.0314)   
      Total_difference         -0.110**       -0.0466***       -0.0957** 
                               (0.0544)        (0.0135)        (0.0381)   
      Total_Explained           0.0210        -0.00877**       -0.0391***
                               (0.0274)        (0.00367)       (0.00874)   
      Total_Unexplained        -0.131**       -0.0379***       -0.0567*  
                               (0.0570)        (0.0135)        (0.0340)   
      ---------------------------------------------------------------------
      Explained
	 (Total composition effect)                        
    
      Total                     0.0210         -0.00877**      -0.0391***
                               (0.0274)        (0.00367)       (0.00874)   
      Pure composition effect  -0.00654        -0.00902**      -0.0453***
                               (0.0194)        (0.00379)       (0.0133)   
      Specification error       0.0276          0.000252        0.00619   
                               (0.0226)        (0.00169)       (0.00678)   
      ---------------------------------------------------------------------
      Pure_explained
	 (Pure composition effect)                                              
      educ                      0.00137        -0.00489*       -0.0263** 
                               (0.0131)        (0.00283)       (0.0106)   
      exper                    -0.00946        -0.00427**      -0.0187***
                               (0.00784)       (0.00173)       (0.00610)   
      tenure                    0.00155         0.000139       -0.000295   
                               (0.0143)        (0.00295)       (0.00778)   
      ---------------------------------------------------------------------
      Specification error                                                
      educ                      0.131           0.00661         0.144*  
                               (0.171)         (0.0212)        (0.0866)   
      exper                     0.131***        0.0204***       0.0676***
                               (0.0467)        (0.00621)       (0.0183)   
      tenure                   -0.0226         -0.00897        -0.0137   
                               (0.0403)        (0.00546)       (0.0145)   
      _cons                    -0.212          -0.0178         -0.192** 
                               (0.192)         (0.0245)        (0.0934)   
      ---------------------------------------------------------------------
      Unexplained 
	 (Total wage structure)                                               
      Total                    -0.131**        -0.0379***      -0.0567*  
                               (0.0570)        (0.0135)        (0.0340)   
      Reweighting error         0.000397       -0.00119        -0.00520   
                               (0.00469)       (0.00179)       (0.00715)   
      Pure wage structure      -0.131**        -0.0367***      -0.0515   
                               (0.0582)        (0.0134)        (0.0328)   
      ---------------------------------------------------------------------
      Pure_unexplained
	 (Pure wage structure)                                             
      educ                     -0.132           0.0687          0.00494   
                               (0.340)         (0.0712)        (0.218)   
      exper                    -0.340***       -0.0346*        -0.0467   
                               (0.101)         (0.0207)        (0.0555)   
      tenure                    0.124           0.0231          0.0256   
                               (0.0823)        (0.0153)        (0.0293)   
      _cons                     0.217          -0.0939         -0.0352   
                               (0.378)         (0.0799)        (0.264)   
      ---------------------------------------------------------------------
      Reweighting error                                              
      educ                     -0.000431        0.000240        0.000991   
                               (0.00225)       (0.00118)       (0.00485)   
      exper                     0.00160        -0.00102        -0.00546   
                               (0.00416)       (0.00125)       (0.00567)   
      tenure                   -0.000776       -0.000408       -0.000730   
                               (0.00356)       (0.000787)      (0.00153)   
      ---------------------------------------------------------------------
      N                          1434             1434            1434               
      Men                         751              751             751
      Women                       683              683             683
      ---------------------------------------------------------------------

由结果可见，男女性收入不平等性的系数效应明显大于特征效应，在分位距、基尼系数上尤为明显：

- 基尼系数的系数效应解释了整体不平等性差异的 81.2%；
- 90-10 分位距的系数效应甚至解释了差异的 119%，因为其特征效应能够减小不平等性。

以上结果表明，在此样本反映的总体中，性别工资不平等的歧视作用较大。

通过特征效应和系数效应的拆解，也可以得到不同解释变量对不平等性差异的贡献程度。以基尼系数衡量不平等性时，男女收入的基尼系数差异中:

-  9.16% (= -0.00427 / -0.0466) 由工作经验的特征效应 (男女平均工作经验不同) 造成
-  74.25% (= -0.0346 / -0.0466) 由工作经验的系数效应 (男女平均工作经验相同，但工资待遇不同) 造成。

以上结果表明，就工作经验角度而言，男女性别工资不平等的部分中，有很大一部分 (74.25%) 是由于工作经验相同、相应待遇不同的歧视效应造成的，而由于男女平均工作经验本身的差异造成的不平等部分占比较小 (9.16%) 。

`oaxaca_rif` 的语法结构如下：

    oaxaca_rif depvar [indepvars] [if] [in] [weight], ///
    by(groupvar) rif(RIF_options) [options]

- `by(groupvar)` : 指明分组变量，如上例中的 **female** ;
-  `rif(RIF_options)` : 计算 RIF 的统计量，与 `rifvar()` 中使用方法相同

`options` 中包括：`noisily` (输出模型估计) 、`swap` (交换组别) 、`wgt()` (指明反事实组的构造，取 0 时 $v_c = \overline   X^{1'}\hat{\beta}^{0} $ ，取 1 时 $v_c = \overline   X^{0'}\hat{\beta}^{1} $ ) 等。


&emsp;

## 5. 注意事项

由于分配权重调整 (reweighting adjustment) 过程得到的是估计出的结果，作者建议在 `oaxaca_rif` 中采用 Bootstrap 标准误。同时，如果统计量涉及无条件分位数，使用 `rifhdreg` 时也应使用 Bootstrap 标准误。具体用法是在执行命令时加上 `bootstrap` 前缀，并指明相应的次数和种子值，例如：

    . bootstrap, reps(500) seed(101):    ///
                 rifhdreg lnwage female, ///
                 over(female) rif(iqr(90 10))  ///
                 robust rwprobit($xvar)
    
    . bootstrap, reps(500) seed(101):    ///
                 oaxaca_rif lnwage educ exper tenure, ///
                 rif(iqr(90 10)) by(female)  ///
                 rwlogit(educ exper tenure)

&emsp;
 
## 6. 参考资料
- Fernando Rios-Avila. 2020. Recentered influence functions (RIFS) in Stata: RIF regression and RIF decomposition. _The Stata Journal_ 1: 51-94. [-PDF-](https://sci-hub.do/10.1177/1536867X20909690)
- 胡雨霄，连享会推文，[性别收入差距=歧视？Oaxaca-Blinder分解方法](https://www.lianxh.cn/news/628d565acfb49.html)
- 胡雨霄，连享会推文，[R2分解：相对重要性分析 (Dominance Analysis)](https://www.lianxh.cn/news/845b935d8d599.html)