# combinatorics 命令介绍及模型筛选


&emsp;

> **作者**：刘佳鹏 (中山大学)    
> **E-Mail:** <sysuliujiapeng@163.com> 

&emsp;

&emsp;

---

[toc]

---

&emsp;

&emsp;

&emsp;

 ## 1. 背景介绍
 “消费者对两种商品组合的偏好，明显强于另一种”，这是我们在学习微观经济学时学到的凸性偏好的定义。我们围绕着消费者效用最大化这个核心概念，去寻找不同情况下能达到效用最大化的最优消费束。而在实证研究中，我们也一样，面对众多可能的解释变量，我们应该保留什么，淘汰什么，我们应该如何从众多组合中找到实现“效用最大化”的最佳组合，也就是我们所说的**模型筛选**。

本推文将要介绍的命令 `combinatorics` 能帮助我们在 Stata 中实现模型筛选，不仅能进行**批量 OLS 估计**，还能支持**留一交叉验证**（LOOCV，Leave-One-Out-Cross-Validation）和**样本外验证**（out-of-sample validation） ，是利用 Stata 处理模型筛选问题的有力工具。本推文的余下部分安排如下：在第二部分，对该命令的基本思想和原理、涉及的主要概念和部分要点进行简单的理论介绍；在第三部分，对该命令的安装方法和语法结构进行说明；在第四部分，将简要介绍该命令的 Stata 实现，一个例子是结合留一交叉验证的应用，另一个例子是结合样本外验证的应用；在第五部分，对本推文主要内容进行总结。
 
 ## 2. 原理介绍
 该数据挖掘程序对由 n 个候选解释变量组成的所有 2^n 个模型执行批量 OLS 估计、样本外验证和留一交叉验证（LOOCV），用于评估所有可能的模型的 OLS **解释性能**和样本外（OOS）**预测性能**。通俗来说，就是将所有候选解释变量进行**排列组合**，列出可能出现的情况总数。
 
 在执行 `combinatorics` 命令，列出所有可能的模型后，需要绘制模型的解释能力和预测能力（R2和LOOCV Pseudo-R²）与模型复杂性（Rank）的关系，这时便需要应用留一交叉验证或者样本外验证。我们首先了解一下留一交叉验证与样本外验证。

### 2.1 留一交叉验证
正常训练会划分训练集和验证集，训练集用来训练模型，而验证集用来评估模型的泛化能力。留一交叉验证是交叉验证一个**极端**的例子，如果数据集 D 的大小为 N ,那么用 N-1 条数据进行训练，用剩下的一条数据作为验证，而用一条数据作为验证可能会得到 Eval 和 Eout 相差很大的结果，所以我们在留一交叉验证里，每次都从 D 中取一组作为验证集，直到所有样本都作过验证集，共计算 N 次，最后对验证误差求平均，得到 Eloocv（H,A），这种方法称之为留一法交叉验证。
$$
\frac{1}{N} \sum_{n=1}^{N} e_{n}=\frac{1}{N} \sum_{n=1}^{N} \operatorname{err}\left(g_{n}^{-}\left(x_{n}\right), y_{n}\right)
$$

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/刘佳鹏_模型筛选_Fig01.png)

#### 2.1.1 LOOCV 理论证明
为什么可以使用 LOOCV 来近似估计泛化误差呢，即
$$
E_{\text {loocv }} \approx E_{\text {out }}
$$
证明如下：
$$
\begin{aligned}
\mathcal{E} E_{\text {loocv }}(\mathcal{H}, \mathcal{A})=\mathcal{E}_{\mathcal{D}} \frac{1}{N} \sum_{n=1}^{N} e_{n} &=\frac{1}{N} \sum_{n=1}^{N} \mathcal{E}_{\mathcal{D}} e_{n} \\
&=\frac{1}{N} \sum_{n=1}^{N} \mathcal{E}_{n\left(\mathbf{x}_{n}, y_{n}\right)} \operatorname{err}\left(g_{n}^{-}\left(\mathbf{x}_{n}\right), y_{n}\right) \\
&=\frac{1}{N} \sum_{n=1}^{N} \mathcal{E}_{\mathcal{D}_{n}} E_{\text {out }}\left(g_{n}^{-}\right) \\
&=\frac{1}{N} \sum_{n=1}^{N} \overline{E_{\text {out }}}(N-1)=\overline{E_{\text {out }}}(N-1)
\end{aligned}
$$
解释如下：
$
\varepsilon_{D}
$ 表示在不同数据集上的期望，$
\varepsilon
$ 是表示期望的符号，这里不同的数据集 D ，是对原始数据的 N 次划分得到的不同数据集 ​$
\varepsilon_{D} e_{n}=>\varepsilon_{D_{n}} \varepsilon_{\left(x_{n}, y_{n}\right)} e_{n}
$ 是因为 Dn 和 （xn,yn） 服从 iid 分布
$
\varepsilon_{\left(x_{n}, y_{n}\right)} \operatorname{err}\left(g_{n}^{-}\left(x_{n}\right), y_{n}\right)
$ 转化为 $
E_{\text {out }}\left(g_{n}^{\overline{}})\right.
$ 是期望的定义和 $
E_{\text {out }}
$ 的定义，虽然是对剩下的一个求误差，但是求的是对剩下单个数据的期望，单个数据的的期望相当于对所有未知数据上求概率平均，也就是 $
g_{n}^{-}
$ 在未知数据上的 E 即 $
E_{\text {out }}
$

#### 2.1.2 为什么使用 LOOCV
第一，交叉验证可以多次的使用数据，有助于解决数据不充足的问题，而 LOOCV 则能**完全充分利用数据**；
第二，有助于防止**过度拟合**（模型样本内拟合的很好，样本外却很糟糕）；
第三，评估模型的**泛化能力**，进行模型的评价与选择。

#### 2.1.3 什么时候使用 LOOCV
当数据集 D 的数量较少时，应该使用留一交叉验证，其原因主要如下：
数据集少，如果像正常一样划分训练集和验证集进行训练，那么可以用于训练的数据本来就少，还被划分出去一部分，这样可以用来训练的数据就更少了。而 LOOCV 可以充分的利用数据。
因为 LOOCV 需要划分 N 次，产生 N 批数据，所以在一轮训练中，要训练出 N 个模型，这样训练时间就大大**增加**。所以 LOOCV 比较适合训练集较少的场景

#### 2.1.4 LOOCV 的优缺点
优点：
充分利用数据
因为采样是确定的，所以最终误差也是确定的，不需要重复 LOOCV
缺点：
训练起来耗时
模型的数量会随着可能解释变量的数量而增加，最好限制在 20 个独立变量的最大值（即 1,048,576 个模型）
由于每次只采一个样本作为验证，导致无法分层抽样，影响验证集上的误差。举个例子，数据集中有数量相等的两个类，对于一条随机数据，他的预测结果是被预测为多数的结果，如果每次划出一条数据作为验证，则其对应的训练集中则会少一条，导致训练集中该条数据占少数从而被预测为相反的类，这样原来的误差率为 50% ，在 LOOCV 中则为 100% 。

#### 2.1.5 LOOCV 模型选择问题
在留一交叉验证过后，到底选择哪一个模型作为最终的模型呢？
考虑一般的划分训练，train_data 七三划分为训练集和验证集，然后每一轮训练都会得到一个 Eval ，训练到 Eval 最低为止，此时的模型就是最终的模型。LOOCV 也是这样的，只不过原来每一轮的训练对应于 LOOCV 中划分 N 次训练 N 个模型，原来的 Eval 对应于 LOOCV 每一轮 N 个误差的平均，这样一轮轮下来直到验证集上的误差最小，此时的模型就是最终需要的模型。

### 2.2 样本外验证
样本外验证（out-of-sample validation）指其中需要事先随机预留部分样本，用于模型建立后对模型进行预测和检验，对比预测结果和实际情况的差异，从而评估模型的预测性能。
例如在预测季度通货膨胀率变动的模型中，样本的数据为 1962-2010 年的季度通货膨胀率变动数据，但我们可以通过保留 2004 年前的样本，然后通过回归模型预测 2005 年及以后年份的季度通货膨胀变动。
而在判断模型的可靠性时，我们不能使用标准误差，即不能使用样本内的数据，而是应该使用样本外的数据。
还是上述提到的季度通货膨胀率变动的模型中，我们应该使用 2005 年以后的季度通货膨胀变动数据的预测值和实际值之间的差异来判断该模型的可靠程度。通过样本外数据计算的均值方差根越小，说明模型对未来的预测能力越好。而在本文中，我们采用 OOS Pseudo-R² 程序，即 OOS 预测值与实际值的相关系数的平方。它可以用与 LOOCV Pseudo-R² 相同的方式作为预测性能的度量。
样本外验证的优点是节省时间和计算成本，缺点也很明显，拟合效果相对留一交叉验证来说没那么好，会出现恰好抽取的样本与模型的拟合效果极差的情况。

### 2.3 留一交叉验证和样本外验证的对比
用 OOS Pseudo-R² 更容易传达模型的结果，但 LOOCV Pseudo-R² 显然是一个更好的程序，因为它不需要多余的观察，也不依赖于对验证样本和样本大小的任意或随机选择。例如，如果我们选择另一个验证样本，OOS Pseudo-R² 可能会给出不同的结论，但 LOOCV Pseudo-R² 会继续显示过拟合。

## 3. 安装命令及语法结构
安装命令：
```Stata
ssc install combinatorics
```
也可以采用以下命令：
```Stata
findit combinatorics
```
点击如下图所示的第二个链接进行安装即可。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/刘佳鹏_模型筛选_Fig02.png)

安装完毕后，可以输入以下命令：
```Stata
help combinatorics
```
会跳转出使用该命令的指导手册，可以通过这个指导手册全面、具体地学习该命令的使用。


`combinatorics` 的语法结构如下：
```Stata
combinatorics depvar indepvars [if] [in] [weight] [, options]
```
**depvar**：解释变量

**indepvar**：被解释变量


执行命令后，结果数据集会包括 2^n 行，每个评估模型对应一行。对于每个模型，会显示如下所示的变量。

模型的特征：

**i**：模型识别号

**model**：以可读形式说明模型包含的变量（需 Stata 版本在 13 及以上）

**rank**：非共线解释变量的个数（含常数）

**timer**：从程序开始到评估这个模型的时间（以秒为单位）


模型的估计结果：

**n**：该模型 OLS 估计的样本量

**r^2**：衡量模型的解释性能

**[Coefficient's name]**：变量的系数，若未估计则记为缺失值。

**[Coefficient's name_SE]**：变量的系数的标准误，若未估计则记为缺失值


模型的样本外预测性能：

**pseudor2**：LOOCV 的 pseudo-R^2，由 hat 矩阵近似估计，在估计子样本中计算

**rmse**：预测的均方根误差，由 LOOCV 程序计算

**oosn**：验证子样本的样本量，它是估计子样本的补充子样本。如果没有 [if] [in] 选项约束估计子样本，则会缺失

**oosr2**：通过预测变量与实际因变量（pseudo-R2）之间的相关系数的平方来衡量模型在验证子样本中的预测性能。如果没有 [if] [in] 选项约束估计子样本，则会缺失

## 4. Stata 实操
打开 1978 年的汽车数据集，结合所有 10 个特征评估汽车价格的1024个模型:
```Stata
sysuse auto,clear
combinatorics price mpg i.rep78 headroom-foreign
```
在得到的数据集中，绘制所有模型得到的 512 个 mpg 系数的直方图：
```Stata
hist mpg
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/刘佳鹏_模型筛选_Fig03.png)


绘制模型解释性能和预测性能（R2 and LOOCV Pseudo-R2) 与模型复杂性（Rank）的关系:
```Stata
twoway (scatter pseudor2 r2 rank,jitter(5 5))(lpolyci pseudor2 rank)(lpolyci r2 rank),xline(6)
```
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/刘佳鹏_模型筛选_Fig04.png)


LOOCV Pseudo-R2 不会随着模型复杂性单调增加（它在 Rank 为 6 时达到局部最大值），在模型选择中存在过拟合和简约的风险。


再次打开 1978 年的汽车数据集，并评估与上面相同的模型，但这次只对 90% 的样本进行随机选择:
```Stata
sysuse auto,clear
set seed 100
gen double oos=(runiform()>0.9)
combinatorics price mpg i.rep78 headroom-foreign if !oos
```


剩下的 10% 用于“样本外”（OOS）预测：
预测来自于它们的估计样本，并根据实际值进行测试；OOS Pseudo-R² 是 OOS 预测值与实际值的相关系数的平方。
它可以用与 LOOCV Pseudo R² 相同的方式作为预测性能的度量:
```Stata
twoway (lpolyci r2 rank)(lpolyci pseudor2 rank)(lpolyci oosr2 rank)
```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/刘佳鹏_模型筛选_Fig05.png)

用 OOS Pseudo-R² 更容易传达模型的结果，但 LOOCV Pseudo-R² 显然是一个更好的程序，因为它不需要多余的观察，也不依赖于对验证样本和样本大小的任意或随机选择。

例如，如果我们选择另一个验证样本，OOS Pseudo-R² 可能会给出不同的结论，但 LOOCV Pseudo-R² 会继续显示过拟合：
```Stata
sysuse auto,clear
set seed 200
gen double oos=(runiform()>0.9)
combinatorics price mpg i.rep78 headroom-foreign if !oos
twoway (lpolyci r2 rank)(lpolyci pseudor2 rank)(lpolyci oosr2 rank)
```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/刘佳鹏_模型筛选_Fig06.png)

## 5. 总结
本文简要的介绍了用 `combinatorics` 命令及应用留一交叉验证或者样本外验证来进行模型筛选。值得注意的是，在实际操作中，使用哪一种验证方法具体取决于实际需要，需要综合考虑准确性、耗时等等问题。随着研究范围的广阔发展及深度的不断加深，好的模型显得尤为重要，模型筛选也就成为重中之重，值得我们进一步的思考、学习和实践应用。

## 6. 参考文献

**温馨提示：** 文中链接在微信中无法生效。请点击底部<font color=red>「阅读原文」</font>。

Charles Lindsey, Simon Sheather, 2010, Variable Selection in Linear Regression, Stata Journal, 10(4): 650–669. [-PDF-](https://journals.sagepub.com/doi/pdf/10.1177/1536867X1101000407)

贺旭，连享会推文，[Stata：交叉验证简介](https://www.lianxh.cn/news/899f4de52f8a3.html)

周志华，机器学习，清华大学出版社，2016

李航，统计学习方法，清华大学出版社，2012

CSDN博客，作者名：很吵请安青争，[LOOCV - Leave-One-Out-Cross-Validation 留一交叉验证](https://blog.csdn.net/dpengwang/article/details/84934197)



&emsp;